<?php







/**







 * mazadd functions and definitions







 *







 * @link https://developer.wordpress.org/themes/basics/theme-functions/







 *







 * @package mazadd







 */















if ( ! function_exists( 'mazadd_setup' ) ) :







	/**







	 * Sets up theme defaults and registers support for various WordPress features.







	 *







	 * Note that this function is hooked into the after_setup_theme hook, which







	 * runs before the init hook. The init hook is too late for some features, such







	 * as indicating support for post thumbnails.







	 */







	function mazadd_setup() {







		/*







		 * Make theme available for translation.







		 * Translations can be filed in the /languages/ directory.







		 * If you're building a theme based on mazadd, use a find and replace







		 * to change 'mazadd' to the name of your theme in all the template files.







		 */







		load_theme_textdomain( 'mazadd', get_template_directory() . '/languages' );















		// Add default posts and comments RSS feed links to head.







		add_theme_support( 'automatic-feed-links' );















		/*







		 * Let WordPress manage the document title.







		 * By adding theme support, we declare that this theme does not use a







		 * hard-coded <title> tag in the document head, and expect WordPress to







		 * provide it for us.







		 */







		add_theme_support( 'title-tag' );















		/*







		 * Enable support for Post Thumbnails on posts and pages.







		 *







		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/







		 */







		add_theme_support( 'post-thumbnails' );















		// This theme uses wp_nav_menu() in one location.







		register_nav_menus( array(







			'menu-1' => esc_html__( 'Primary1', 'mazadd' ),







			'menu-2' => esc_html__( 'Primary2', 'mazadd' ),



			'Footer_left_menu' => esc_html__( 'Footer left menu', 'mazadd' ),







		) );















		/*







		 * Switch default core markup for search form, comment form, and comments







		 * to output valid HTML5.







		 */







		add_theme_support( 'html5', array(







			'search-form',







			'comment-form',







			'comment-list',







			'gallery',







			'caption',







		) );







add_theme_support( 'woocommerce' );







		// Set up the WordPress core custom background feature.







		add_theme_support( 'custom-background', apply_filters( 'mazadd_custom_background_args', array(







			'default-color' => 'ffffff',







			'default-image' => '',







		) ) );















		// Add theme support for selective refresh for widgets.







		add_theme_support( 'customize-selective-refresh-widgets' );















		/**







		 * Add support for core custom logo.







		 *







		 * @link https://codex.wordpress.org/Theme_Logo







		 */







		add_theme_support( 'custom-logo', array(







			'height'      => 250,







			'width'       => 250,







			'flex-width'  => true,







			'flex-height' => true,







		) );







	}







endif;







add_action( 'after_setup_theme', 'mazadd_setup' );















function create_mazadd_tables()







{







	global $wpdb;







     $table_name = $wpdb->prefix . '';







     $wpdb_collate = $wpdb->collate;







     $sql =







         "CREATE TABLE {$table_name} (







         id mediumint(8) unsigned NOT NULL auto_increment ,







         first varchar(255) NULL,







         PRIMARY KEY  (id),







         KEY first (first)







         )







         COLLATE {$wpdb_collate}";







 







     require_once(ABSPATH . 'wp-admin/includes/upgrade.php');







     dbDelta( $sql );







}















/**







 * Set the content width in pixels, based on the theme's design and stylesheet.







 *







 * Priority 0 to make it available to lower priority callbacks.







 *







 * @global int $content_width







 */







function mazadd_content_width() {







	$GLOBALS['content_width'] = apply_filters( 'mazadd_content_width', 640 );







}







add_action( 'after_setup_theme', 'mazadd_content_width', 0 );















/**







 * Register widget area.







 *







 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar







 */







function mazadd_widgets_init() {







	register_sidebar( array(







		'name'          => esc_html__( 'Sidebar', 'mazadd' ),







		'id'            => 'sidebar-1',







		'description'   => esc_html__( 'Add widgets here.', 'mazadd' ),







		'before_widget' => '<section id="%1$s" class="widget %2$s">',







		'after_widget'  => '</section>',







		'before_title'  => '<h2 class="widget-title">',







		'after_title'   => '</h2>',







	) );







}







add_action( 'widgets_init', 'mazadd_widgets_init' );















/**







 * Enqueue scripts and styles.







 */







function mazadd_scripts() {







	wp_enqueue_style( 'mazadd-bootstrap_css', get_template_directory_uri() . '/css/bootstrap.min.css' );

	//wp_enqueue_style( 'mazadd-font-awesome_css', "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" );

	wp_enqueue_style( 'mazadd-style', get_stylesheet_uri() );



	//wp_style_add_data( 'mazadd-style', 'rtl', 'replace' );


	wp_enqueue_script( 'mazadd-jquery', get_template_directory_uri() . '/js/jquery-1.12.4.min.js', array(), '20151215', false );


	wp_enqueue_script( 'mazadd-bootstrap_js', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '20151215', false );
	//
	//

	if(is_page(array('ad-post'))){
		wp_enqueue_style( 'mazadd-datetimepicker1_css', get_template_directory_uri() . '/css/bootstrap-datetimepicker.min.css' );
		wp_enqueue_style( 'mazadd-datetimepicker-standalone_css', get_template_directory_uri() . '/css/bootstrap-datetimepicker-standalone.css' );
		wp_enqueue_style( 'mazadd-bootstrap-datetimepicker_css', get_template_directory_uri() . '/css/bootstrap-datetimepicker.css' );
		//wp_enqueue_script( 'mazadd-mommemtmin_js', get_template_directory_uri() . '/js/moment.min.js', array(), '20151215', false );
		wp_enqueue_script( 'mazadd-mommemt_js', get_template_directory_uri() . '/js/moment.js', array(), '20151215', false );
		wp_enqueue_script( 'mazadd-bootstrap-datetimepicker_js', get_template_directory_uri() . '/js/bootstrap-datetimepicker.min.js', array(), '20151215', false );
	}
	







	//wp_enqueue_script( 'mazadd-ui_js', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js', array(), '20151215', true );
	//wp_enqueue_script( 'mazadd-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	//wp_enqueue_script( 'mazadd-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );















	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {







		wp_enqueue_script( 'comment-reply' );







	}







}







add_action( 'wp_enqueue_scripts', 'mazadd_scripts' );















/**







 * Implement the Custom Header feature.







 */







require get_template_directory() . '/inc/custom-header.php';















/**







 * Custom template tags for this theme.







 */







require get_template_directory() . '/inc/template-tags.php';















/**







 * Functions which enhance the theme by hooking into WordPress.







 */







require get_template_directory() . '/inc/template-functions.php';







require get_template_directory() . '/inc/custom-notification.php';
require get_template_directory() . '/inc/arabic_transliteration.php';















/**







 * Customizer additions.







 */







require get_template_directory() . '/inc/customizer.php';







require get_template_directory() . '/inc/wp_bootstrap_navwalker.php';















/**







 * Load Jetpack compatibility file.







 */







if ( defined( 'JETPACK__VERSION' ) ) {







	require get_template_directory() . '/inc/jetpack.php';







}















function woocommerce_breadcrumb( $args = array() ) {







		$args = wp_parse_args( $args, apply_filters( 'woocommerce_breadcrumb_defaults', array(







			'delimiter'   => ' >> ',







			'wrap_before' => '<span class="posteddetails">',







			'wrap_after'  => '</span>',







			'before'      => '',







			'after'       => '',







			'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' ),







		) ) );















		$breadcrumbs = new WC_Breadcrumb();















		if ( ! empty( $args['home'] ) ) {







			$breadcrumbs->add_crumb( $args['home'], apply_filters( 'woocommerce_breadcrumb_home_url', home_url() ) );







		}















		$args['breadcrumb'] = $breadcrumbs->generate();















		/**







		 * WooCommerce Breadcrumb hook







		 *







		 * @hooked WC_Structured_Data::generate_breadcrumblist_data() - 10







		 */







		do_action( 'woocommerce_breadcrumb', $breadcrumbs, $args );















		wc_get_template( 'global/breadcrumb.php', $args );







	}







	function auction_details_section(){







		wc_get_template( 'single-product/auction-details.php' );







	}







remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );







remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );







remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );







//remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );







remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );







remove_action( 'woocommerce_product_thumbnails', 'woocommerce_show_product_thumbnails', 20 );















add_action( 'breadcrumb_sections', 'woocommerce_template_single_title', 5 );







add_action( 'breadcrumb_sections', 'woocommerce_breadcrumb', 10 );







add_action( 'breadcrumb_sections', 'auction_details_section', 15);















add_action( 'after_breadcrumb_sections', 'woocommerce_template_single_add_to_cart', 5 );







add_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_thumbnails', 20 );























remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );







remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );







add_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );































/*loop section*/















function thumbanail_for_loop(){







	$images = explode(",", get_post_meta(get_the_ID(),'_product_image_gallery',true)) ;







	?>







	<img src='<?php echo wp_get_attachment_image_url( $images[0],'shop_catalog');?>' title='<?php echo get_the_title();?>' class='thumbnailad'>







	<?php







}























/**







 * Template pages







 */















if ( ! function_exists( 'woocommerce_content' ) ) {















	/**







	 * Output WooCommerce content.







	 *







	 * This function is only used in the optional 'woocommerce.php' template.







	 * which people can add to their themes to add basic woocommerce support.







	 * without hooks or modifying core templates.







	 */







	function woocommerce_content() {















		if ( is_singular( 'product' ) ) {















			while ( have_posts() ) : the_post();















				wc_get_template_part( 'content', 'single-product' );















			endwhile;















		} else { ?>















			<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>















			<?php endif; ?>















			<?php //do_action( 'woocommerce_archive_description' ); ?>







<div class="container bs-docs-container">







        <div class="main">







<div id="bodycont">







        







        <div class="row">







            <div class="col-sm-12" style="margin-bottom: 20px;">







    <div class="row">







			<?php if ( have_posts() ) : ?>















				<?php //do_action( 'woocommerce_before_shop_loop' ); ?>















				<?php woocommerce_product_loop_start(); ?>















					<?php //woocommerce_product_subcategories(); ?>















					<?php while ( have_posts() ) : the_post(); ?>















						<?php wc_get_template_part( 'content', 'product' ); ?>















					<?php endwhile; // end of the loop. ?>















				<?php woocommerce_product_loop_end(); ?>







				</div>







				</div>







				<?php do_action( 'woocommerce_after_shop_loop' ); ?>















			<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>















				<?php do_action( 'woocommerce_no_products_found' ); ?>















			<?php endif;







			?>







			















</div>







</div>







</div></div>







			<?php















		}







	}







}































































if ( class_exists( 'YITH_Auction_Frontend_Premium' ) ) {

  /**

  * Class YITH_Auctions_Frontend

 * 



 * @author Carlos Rodríguez <carlos.rodriguez@yourinspiration.it>

 */

class YITH_Auction_Frontend_extends extends YITH_Auction_Frontend_Premium 

	{

	/**

		\* Single instance of the class

         *

         * @var \YITH_Auction_Admin

         * @since 1.0.0

         */

		protected static $instance;

		public static function get_instance() {

            $self = __CLASS__ . ( class_exists( __CLASS__ . '_Premium' ) ? '_Premium' : '' );

            if ( is_null( $self::$instance ) ) {

                $self::$instance = new $self;

            }

            return $self::$instance;

        }



        /**

         * Construct

        *



         * @author Carlos Rodríguez <carlos.rodriguez@yourinspiration.it>



         * @since 1.0

         */



        public function __construct()

        {

           /* add_action('woocommerce_auction_add_to_cart', array($this, 'print_add_to_cart_template'));



            add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));



            add_filter('woocommerce_product_tabs', array($this, 'create_bid_tab'), 999);



            add_action('woocommerce_after_shop_loop_item', array($this, 'auction_end_start'), 8);



            add_filter('woocommerce_product_add_to_cart_text', array($this, 'change_button_auction_shop'), 10, 2);



            add_filter('woocommerce_get_price_html', array($this, 'change_product_price_display'), 10, 2);



            add_filter('woocommerce_empty_price_html', array($this, 'set_empty_product_price'), 10, 2);



            add_filter('woocommerce_free_price_html', array($this, 'set_empty_product_price'), 10, 2);



            add_action('woocommerce_before_shop_loop_item_title', array($this, 'auction_badge_shop'), 10);



            if ( version_compare( WC()->version , '2.7.0', '>=' ) ) {



                add_filter('woocommerce_single_product_image_thumbnail_html', array($this,'add_badge_single_product'));

            } else {



                add_filter('woocommerce_single_product_image_html', array($this, 'add_badge_single_product'));



            }*/



            add_action('woocommerce_login_form_end', array($this, 'add_redirect_after_login'));

            remove_action('yith_wcact_auction_before_set_bid',array($this,'add_auction_timeleft'));



             add_action('yith_wcact_auction_before_set_bid1',array($this,'add_auction_timeleft1'));



 if ( version_compare( WC()->version , '2.7.0', '>=' ) ) {



                remove_filter('woocommerce_single_product_image_thumbnail_html', array($this,'add_badge_single_product'));

                add_filter('woocommerce_single_product_image_thumbnail_html', array($this,'add_badge_single_product1'));



            } else {



                remove_filter('woocommerce_single_product_image_html', array($this, 'add_badge_single_product'));

                add_filter('woocommerce_single_product_image_html', array($this, 'add_badge_single_product1'));

            }

            remove_action('yith_wcact_auction_end',array($this,'auction_end'));

            add_action('yith_wcact_auction_end1',array($this,'auction_end1'));



        }

        /**

         * Auction template9

        *

         * Add the auction template

         *



         * @author Carlos Rodríguez <carlos.rodriguez@yourinspiration.it>

         * @since 1.0

         * @return void

         */

        public function print_add_to_cart_template()

        {



			 wc_get_template('single-product/add-to-cart/auction.php', array(), '', YITH_WCACT_TEMPLATE_PATH . 'woocommerce/');



        }

      /**

         *  Show auction timeleft

         *



         * @author Carlos Rodríguez <carlos.rodriguez@yourinspiration.it>

         * @since 1.0.14

         */







        public function add_auction_timeleft1( $product ) {

            $args = array(

                'product' =>$product,

            );

            wc_get_template('frontend/auction-timeleft.php', $args );

        }







        public function add_badge_single_product1($output)

        {

            global $product;

            $img = YITH_WCACT_ASSETS_URL . '/images/badge.png';

            if ('auction' == $product->get_type() && $img) {

                $output .= '';



            }

            return $output;



        }

        public function auction_end1($product) {

            $instance = YITH_Auctions()->bids;

            $max_bid = $instance->get_max_bid($product->get_id());

            $current_user = wp_get_current_user();

            if ($max_bid && $current_user->ID == $max_bid->user_id) {

                ?>

                <div id="Congratulations">

                    <h2><?php _e('Congratulations, you won this auction', 'yith-auctions-for-woocommerce') ?></h2>

                </div>

                <div style="margin-top: 10px; margin-bottom: 10px;">

                <form class="cart" method="get" enctype='multipart/form-data'>

                    <input type="hidden" name="yith-wcact-pay-won-auction" value="<?php echo esc_attr($product->get_id()); ?>"/>

                    <?php

                    if (!$product->is_paid() && ('yes' == get_option('yith_wcact_settings_tab_auction_show_button_pay_now'))) {

                        ?>

                        <button type="submit" class="auction_add_to_cart_button button alt spnbuttonicon" id="yith-wcact-auction-won-auction">

                            <?php echo sprintf(__('Pay now', 'yith-auctions-for-woocommerce')); ?>

                        </button>

                        <?php

                    }

                    ?>
                </form>

 			</div>

                <?php

            }

        }

    }



   new YITH_Auction_Frontend_extends();

}





function spectro_button_value()







{







	$name=(string) $_POST['name'];







	$id=(int) $_POST['id'];







	$ages = get_categories( array( 'taxonomy' => 'product_cat',







        'hide_empty' => false,







        'parent' => $id,'name__like'=> $name







    ));







	















	echo json_encode($ages);







	die();







	







}







add_action( 'wp_ajax_button_value','spectro_button_value');







add_action( 'wp_ajax_nopriv_button_value','spectro_button_value');















function add_auction_categories(){







	$ex=array();







	$name=(string) $_POST['whatever'];







	$parenttop=array();







	$parents = get_terms( array( 'taxonomy' => 'product_cat', 'parent' => 0,'hide_empty' => false) );







	foreach ($parents as $value) {







		array_push($parenttop, $value->term_id );







	}















	$excludes = get_categories( array( 'taxonomy' => 'product_cat','exclude'=>$parenttop,'fields' => 'all','name__like'=> $name,'childless'=>true,'hide_empty' => false));







	foreach ($excludes as $value) {







		array_push($parenttop, $value->term_id );







	}















	$excludes = get_categories( array( 'taxonomy' => 'product_cat','exclude'=>$parenttop,'fields' => 'all','name__like'=> $name,'hide_empty' => false));







	foreach ($excludes as $exclude) {







		







		array_push($ex, $exclude);







	}







	//var_dump($ex);







	 







   echo json_encode($ex);







    die();







}







add_action( 'wp_ajax_auction_categories','add_auction_categories');







add_action( 'wp_ajax_nopriv_auction_categories','add_auction_categories');















function mazadd_post_type_data($post_type,$count = null,$offset = null){







		$arr_query = array(







			'post_type'           => $post_type,







			'post_status'         => 'publish',







			'ignore_sticky_posts' => true,







			'posts_per_page' => $count,







			'offset' => $offset







		);







	







		







	$my_query  = new WP_Query( $arr_query );







	if ( $my_query->have_posts() ) {







		while ( $my_query->have_posts() ) {







			$p       = $my_query->next_post();







			$posts[] = $p;







		}







	}







	return $posts;







}















function add_auction_model(){







	$id = (int) $_POST['cat_name'];







	$cat = get_term_by( 'id', $id, 'product_cat');















	if($cat->slug == 'car' or $cat->slug == 'سيارة' or $cat->slug == 'bike'){







			get_template_part( 'template-parts/content', 'car' );















	}elseif ($cat->slug == 'real-estate') {







		get_template_part( 'template-parts/content', 'estate');







	}else{







		get_template_part( 'template-parts/content', 'common');







	}







	die();







}































add_action( 'wp_ajax_auction_model','add_auction_model');







add_action( 'wp_ajax_nopriv_auction_model','add_auction_model');















function get_cities_sa(){







		set_time_limit (120);















	global $wpdb;







	$array = array_map('str_getcsv', file(get_template_directory_uri().'/addons/city.csv'));







	$count = count($array);







	$inc=0;







	foreach ($array as $value) {







		$inc++;







		$wpdb->insert( 'wp_monadie_cities', array('City_Name'=>$value[0],'Country_Code'=>$value[1]) , array( '%s', '%s' )  ); 







		if ($count == $inc) {







			die();







		}







	}







	/*$header = array_shift($array);







	print_r($array);*/







}







function get_regions_sa(){







	global $wpdb;







	$array = array_map('str_getcsv', file(get_template_directory_uri().'/addons/region_sa.csv'));







	$count = count($array);







	$inc=0;







	foreach ($array as $value) {







		$inc++;







		$wpdb->insert( 'wp_monadie_regions', array('Region_Name'=>$value[0],'Country_Code'=>$value[1]) , array( '%s', '%s' )  ); 







		if ($count == $inc) {







			die();







		}







	}







	/*$header = array_shift($array);







	print_r($array);*/







}







/*add_action( 'wp_ajax_get_cities','get_cities_sa');







add_action( 'wp_ajax_nopriv_get_cities','get_cities_sa');*/















function get_cities()
{
	$lan = new GoogleTranslate();

	
	$cities=array();

	global $wpdb;
	$country = $_POST['country'];

	$fivesdrafts = $wpdb->get_results( 
	"SELECT * FROM wp_monadie_cities WHERE Country_Code = '$country'");
	//var_dump($fivesdrafts);
	foreach ($fivesdrafts as $value) {
		$city=array();
		if($my_current_lang = apply_filters( 'wpml_current_language', NULL ) == 'en' ){ 
			$City_Name = $value->City_Name;
		}else{
			$City_Name = $lan->translate('en','ar',$value->City_Name);
		}
		$city['id']=$value->id;
		$city['City_Name']=$City_Name;
		$city['Country_Code']=$value->Country_Code;
		array_push($cities, $city);
	}
		echo json_encode($cities);
	die();
}
add_action( 'wp_ajax_cities','get_cities');
add_action( 'wp_ajax_nopriv_cities','get_cities');







function get_regions()







{







	global $wpdb;







	$country = $_POST['country'];







	$fivesdrafts = $wpdb->get_results( 







	"SELECT * FROM wp_monadie_regions WHERE Country_Code = '$country'");







	







		echo json_encode($fivesdrafts);







	







	die();







}







add_action( 'wp_ajax_regions','get_regions');







add_action( 'wp_ajax_nopriv_regions','get_regions');







function create_city_table(){







	global $wpdb;















	$charset_collate = $wpdb->get_charset_collate();







if($wpdb->get_var("SHOW TABLES LIKE 'wp_monadie_cities'") != 'wp_monadie_cities') {







	$sql = "CREATE TABLE wp_monadie_cities (







	  id int(9) NOT NULL AUTO_INCREMENT,







	  City_Name text NOT NULL,







	  Country_Code varchar(55)  NOT NULL,







	  PRIMARY KEY  (id)







	) $charset_collate;";















	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );







	dbDelta( $sql );







	get_cities_sa();







	}







}















function create_region_table(){







	global $wpdb;







if($wpdb->get_var("SHOW TABLES LIKE 'wp_monadie_regions'") != 'wp_monadie_regions') {







	$charset_collate = $wpdb->get_charset_collate();















	$sql = "CREATE TABLE wp_monadie_regions (







	  id int(9) NOT NULL AUTO_INCREMENT,







	  Region_Name text NOT NULL,







	  Country_Code varchar(55)  NOT NULL,







	  PRIMARY KEY  (id)







	) $charset_collate;";















	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );







	dbDelta( $sql );







	get_regions_sa();







}







}







//create_region_table();







//create_city_table();















if ( ! function_exists( 'woocommerce_form_field' ) ) {















	/**







	 * Outputs a checkout/address form field.







	 *







	 * @subpackage    Forms







	 *







	 * @param string $key Key.







	 * @param mixed  $args Arguments.







	 * @param string $value (default: null).







	 *







	 * @return string







	 */







	function woocommerce_form_field( $key, $args, $value = null ) {







		$defaults = array(







			'type'              => 'text',







			'label'             => '',







			'description'       => '',







			'placeholder'       => '',







			'maxlength'         => false,







			'required'          => false,







			'autocomplete'      => false,







			'id'                => $key,







			'class'             => array(),







			'label_class'       => array(),







			'input_class'       => array(),







			'return'            => false,







			'options'           => array(),







			'custom_attributes' => array(),







			'validate'          => array(),







			'default'           => '',







			'autofocus'         => '',







			'priority'          => '',







		);















		$args = wp_parse_args( $args, $defaults );







		$args = apply_filters( 'woocommerce_form_field_args', $args, $key, $value );















		if ( $args['required'] ) {







			$args['class'][] = 'validate-required';







			$required = ' <abbr class="required" title="' . esc_attr__( 'required', 'woocommerce' ) . '">*</abbr>';







		} else {







			$required = '';







		}















		if ( is_string( $args['label_class'] ) ) {







			$args['label_class'] = array( $args['label_class'] );







		}















		if ( is_null( $value ) ) {







			$value = $args['default'];







		}















		// Custom attribute handling.







		$custom_attributes         = array();







		$args['custom_attributes'] = array_filter( (array) $args['custom_attributes'] );















		if ( $args['maxlength'] ) {







			$args['custom_attributes']['maxlength'] = absint( $args['maxlength'] );







		}















		if ( ! empty( $args['autocomplete'] ) ) {







			$args['custom_attributes']['autocomplete'] = $args['autocomplete'];







		}















		if ( true === $args['autofocus'] ) {







			$args['custom_attributes']['autofocus'] = 'autofocus';







		}















		if ( ! empty( $args['custom_attributes'] ) && is_array( $args['custom_attributes'] ) ) {







			foreach ( $args['custom_attributes'] as $attribute => $attribute_value ) {







				$custom_attributes[] = esc_attr( $attribute ) . '="' . esc_attr( $attribute_value ) . '"';







			}







		}















		if ( ! empty( $args['validate'] ) ) {







			foreach ( $args['validate'] as $validate ) {







				$args['class'][] = 'validate-' . $validate;







			}







		}















		$field           = '';







		$label_id        = $args['id'];







		$sort            = $args['priority'] ? $args['priority'] : '';







		$field_container = '<p class="form-row %1$s" id="%2$s" data-priority="' . esc_attr( $sort ) . '">%3$s</p>';















		switch ( $args['type'] ) {







			case 'country' :















				$countries = 'shipping_country' === $key ? WC()->countries->get_shipping_countries() : WC()->countries->get_allowed_countries();















				if ( 1 === count( $countries ) ) {















					$field .= '<strong>' . current( array_values( $countries ) ) . '</strong>';















					$field .= '<input type="hidden" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" value="' . current( array_keys( $countries ) ) . '" ' . implode( ' ', $custom_attributes ) . ' class="country_to_state" readonly="readonly" />';















				} else {















					$field = '<select name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" class="country_to_state country_select ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" ' . implode( ' ', $custom_attributes ) . '><option value="">' . esc_html__( 'Select a country&hellip;', 'woocommerce' ) . '</option>';















					foreach ( $countries as $ckey => $cvalue ) {







						$field .= '<option value="' . esc_attr( $ckey ) . '" ' . selected( $value, $ckey, false ) . '>' . $cvalue . '</option>';







					}















					$field .= '</select>';















					$field .= '<noscript><input type="submit" name="woocommerce_checkout_update_totals" value="' . esc_attr__( 'Update country', 'woocommerce' ) . '" /></noscript>';















				}















				break;







			case 'state' :







				/* Get country this state field is representing */







				$for_country = isset( $args['country'] ) ? $args['country'] : WC()->checkout->get_value( 'billing_state' === $key ? 'billing_country' : 'shipping_country' );







				$states      = WC()->countries->get_states( $for_country );















				if ( is_array( $states ) && empty( $states ) ) {















					$field_container = '<p class="form-row %1$s" id="%2$s" style="display: none">%3$s</p>';















					$field .= '<input type="hidden" class="hidden" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" value="" ' . implode( ' ', $custom_attributes ) . ' placeholder="' . esc_attr( $args['placeholder'] ) . '" readonly="readonly" />';















				} elseif ( ! is_null( $for_country ) && is_array( $states ) ) {















					$field .= '<select name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" class="state_select form-control ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" ' . implode( ' ', $custom_attributes ) . ' data-placeholder="' . esc_attr( $args['placeholder'] ) . '">







						<option value="">' . esc_html__( 'Select a state&hellip;', 'woocommerce' ) . '</option>';















					foreach ( $states as $ckey => $cvalue ) {







						$field .= '<option value="' . esc_attr( $ckey ) . '" ' . selected( $value, $ckey, false ) . '>' . $cvalue . '</option>';







					}















					$field .= '</select>';















				} else {















					$field .= '<input type="text" class="form-control ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" value="' . esc_attr( $value ) . '"  placeholder="' . esc_attr( $args['placeholder'] ) . '" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" ' . implode( ' ', $custom_attributes ) . ' />';















				}















				break;







			case 'textarea' :















				$field .= '<textarea name="' . esc_attr( $key ) . '" class="form-control ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" id="' . esc_attr( $args['id'] ) . '" placeholder="' . esc_attr( $args['placeholder'] ) . '" ' . ( empty( $args['custom_attributes']['rows'] ) ? ' rows="2"' : '' ) . ( empty( $args['custom_attributes']['cols'] ) ? ' cols="5"' : '' ) . implode( ' ', $custom_attributes ) . '>' . esc_textarea( $value ) . '</textarea>';















				break;







			case 'checkbox' :















				$field = '<label class="checkbox ' . implode( ' ', $args['label_class'] ) . '" ' . implode( ' ', $custom_attributes ) . '>







						<input type="' . esc_attr( $args['type'] ) . '" class="input-checkbox ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" value="1" ' . checked( $value, 1, false ) . ' /> '







						 . $args['label'] . $required . '</label>';















				break;







			case 'password' :







			case 'text' :







			case 'email' :







			case 'tel' :







			case 'number' :















				$field .= '<input type="' . esc_attr( $args['type'] ) . '" class="form-control ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" placeholder="' . esc_attr( $args['placeholder'] ) . '"  value="' . esc_attr( $value ) . '" ' . implode( ' ', $custom_attributes ) . ' />';















				break;







			case 'select' :















				$options = $field = '';















				if ( ! empty( $args['options'] ) ) {







					foreach ( $args['options'] as $option_key => $option_text ) {







						if ( '' === $option_key ) {







							// If we have a blank option, select2 needs a placeholder.







							if ( empty( $args['placeholder'] ) ) {







								$args['placeholder'] = $option_text ? $option_text : __( 'Choose an option', 'woocommerce' );







							}







							$custom_attributes[] = 'data-allow_clear="true"';







						}







						$options .= '<option value="' . esc_attr( $option_key ) . '" ' . selected( $value, $option_key, false ) . '>' . esc_attr( $option_text ) . '</option>';







					}















					$field .= '<select name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" class="select' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" ' . implode( ' ', $custom_attributes ) . ' data-placeholder="' . esc_attr( $args['placeholder'] ) . '">







							' . $options . '







						</select>';







				}















				break;







			case 'radio' :















				$label_id = current( array_keys( $args['options'] ) );















				if ( ! empty( $args['options'] ) ) {







					foreach ( $args['options'] as $option_key => $option_text ) {







						$field .= '<input type="radio" class="input-radio ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" value="' . esc_attr( $option_key ) . '" name="' . esc_attr( $key ) . '" ' . implode( ' ', $custom_attributes ) . ' id="' . esc_attr( $args['id'] ) . '_' . esc_attr( $option_key ) . '"' . checked( $value, $option_key, false ) . ' />';







						$field .= '<label for="' . esc_attr( $args['id'] ) . '_' . esc_attr( $option_key ) . '" class="radio ' . implode( ' ', $args['label_class'] ) . '">' . $option_text . '</label>';







					}







				}















				break;







		}















		if ( ! empty( $field ) ) {







			$field_html = '';















			if ( $args['label'] && 'checkbox' != $args['type'] ) {







				$field_html .= '<label for="' . esc_attr( $label_id ) . '" class="' . esc_attr( implode( ' ', $args['label_class'] ) ) . '">' . $args['label'] . $required . '</label>';







			}















			$field_html .= $field;















			if ( $args['description'] ) {







				$field_html .= '<span class="description">' . esc_html( $args['description'] ) . '</span>';







			}















			$container_class = esc_attr( implode( ' ', $args['class'] ) );







			$container_id    = esc_attr( $args['id'] ) . '_field';







			$field           = sprintf( $field_container, $container_class, $container_id, $field_html );







		}















		$field = apply_filters( 'woocommerce_form_field_' . $args['type'], $field, $key, $args, $value );















		if ( $args['return'] ) {







			return $field;







		} else {







			echo $field; // WPCS: XSS ok.







		}







	}







}







if(!is_admin()){

	show_admin_bar( false );

}







/*global $wp_roles; // global class wp-includes/capabilities.php







  $wp_roles->add_cap('yith_vendor', 'read' ); */







 /* 'read' => ' 1' */







 







 /*$parents = get_terms( array( 'taxonomy' => YITH_Vendors()->get_taxonomy_name(), 'parent' => 0,'hide_empty' => false) );







	foreach ($parents as $value) {







		var_dump($value);







	}







	$auction = get_term_by('slug', 'vivekkumar',YITH_Vendors()->get_taxonomy_name());







	var_dump();*/







function add_capabilities(){







	global $wp_roles; // global class wp-includes/capabilities.php







  $wp_roles->add_cap('customer', 'edit_product_terms' ); 







  $wp_roles->add_cap('customer', 'delete_product_terms' ); 







  $wp_roles->add_cap('customer', 'assign_product_terms' ); 







}







function mazadd_theme_after_setup(){







	create_city_table();







	create_region_table();







	add_capabilities();







	create_notification_table();







	create_online_user_table();







	create_message_table();







}







add_action( 'after_setup_theme', 'mazadd_theme_after_setup' );















function create_online_user_table(){







	global $wpdb;















	$charset_collate = $wpdb->get_charset_collate();







	$table_name = $wpdb->prefix.'online_user';







	if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {







		$sql = "CREATE TABLE $table_name (







		  id int(9) NOT NULL AUTO_INCREMENT,







		  user_id varchar(55) NOT NULL,







		  online_status varchar(55)  NOT NULL,







		  PRIMARY KEY  (id)







		) $charset_collate;";















		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );







		dbDelta( $sql );







	}







}







add_action( 'wp_logout', 'auto_redirect_external_after_logout');







function auto_redirect_external_after_logout(){







 echo 'hai'.get_current_user_id();



session_destroy();



 global $wpdb; 







	$table_name = $wpdb->prefix.'online_user';







  $wpdb->update( 







	$table_name, array( 'online_status' => 0), array( 'user_id' => get_current_user_id(), 'online_status'=>1 ), array( '%d'),array( '%d','%d') 







);

/*
  if(apply_filters( 'wpml_current_language', NULL ) == 'en' ){ 
  	wp_redirect(site_url().'/login/?l=my-account');
  }else{
  	wp_redirect(site_url().'/ar/تسجيل-الدخول/?l=حسابي');
  }
	

	 exit();*/







}















function your_function( $user_login, $user ) {







  global $wpdb; 







	$table_name = $wpdb->prefix.'online_user';







  $wpdb->update( 







	$table_name, array( 'online_status' => 1), array( 'user_id' => $user->id, 'online_status'=>0 ), array( '%d'),array( '%d','%d') 







);

  session_start();

get_auction_cats();

get_auction_subcats();





}





add_action('wp_login', 'get_auction_cats', 10, 2);

add_action('wp_login', 'get_auction_subcats', 10, 2);

add_action('wp_login', 'your_function', 10, 2);
















add_action( 'user_register', 'myplugin_registration_save', 10, 1 );















function myplugin_registration_save( $user_id ) {







	global $wpdb; 







	$table_name = $wpdb->prefix.'online_user';







    $wpdb->insert( $table_name, array('user_id'=>$user_id,'online_status'=>0) , array( '%d', '%d' )  ); 















}















function get_auction_cats(){



$lan = new GoogleTranslate();




	$ex=array();







	$parents = get_terms( array( 'taxonomy' => 'product_cat', 'parent' => 0,'hide_empty' => false) );







	foreach ($parents as $value) {

	$excludes = get_categories( array( 'taxonomy' => 'product_cat','parent'=>$value->term_id,'hide_empty' => false));

	foreach ($excludes as $exclude) {
/*echo $exclude->cat_name;
echo $name = $lan->translate('en','ar',$exclude->cat_name);*/
	/*if($my_current_lang = apply_filters( 'wpml_current_language', NULL ) == 'ar'){
		echo $name = $lan->EnToArPhonetic($exclude->cat_name);
	}else{
		$name = $exclude->cat_name;
	}*/
$name = $exclude->cat_name;
			array_push($ex, array('term_id' => $exclude->term_id,'name' => $name,'parent'=> $exclude->parent));

	}







	}



	$_SESSION['category']=json_encode($ex);

}







function get_auction_subcats(){

	$ex2=array();

	$parents2 = json_decode($_SESSION['category']);

	foreach ($parents2 as $value) {



	$excludes2 = get_categories( array( 'taxonomy' => 'product_cat','child_of'=>$value->term_id,'hide_empty' => false));

		foreach ($excludes2 as $exclude) {

			$term = get_term_by( 'id', $value->term_id, 'product_cat');
			/*echo $name = $lan->translate('en','ar',$exclude->cat_name);
			echo $pname = $lan->translate('en','ar',$term->name);*/
			$name = $exclude->cat_name;
			$pname = $term->name;
			array_push($ex2, array('term_id' => $exclude->term_id,'name' => $name,'parent'=> $pname,'parent_id'=>$value->term_id));

		}



	}

	$_SESSION['sub_category']=json_encode($ex2);

}























function print_spectro_scripts() { ?>



	<script>

jQuery('#regiter_show').click(function(){
			$('#register').show();
			$('#logins').hide();
		})
	/*var timer;
	$(document).ready(function(){
    $("input").keydown(function(){
        $("input").css("background-color", "yellow");

    });
    $("#query").keyup(function(){
    	if(timer)
    	{
    	clearTimeout(timer);
    	}
    	var value=document.getElementById("query").value;
    	timer = setTimeout(function(){myFunction(value);}, 2000);
     	//timer= setTimeout(myFunction( value),3000); 
    });
	});*/
</script>
<?php if( is_page( array( 'ad-post','اضف اعلان','search','بحث في الاعلانات' ) ) ){ ?>
<script type="text/javascript">

	function check(value)
	{
		localStorage.setItem("id",value);
		check_subBy_id(parseInt(value));
		//alert(document.getElementById("selection").value);
	}
	var mainJsonObject;
var subJSonObject;
var selectedJSonObj;
var selectedSubObject;
var searchLetter="";
<?php if($_SESSION['category']){echo 'mainJsonObject ='.$_SESSION['category'].';'; }?>
 <?php if($_SESSION['sub_category']) {echo 'subJSonObject ='. $_SESSION['sub_category'].';'; } ?> 

console.log(mainJsonObject);
console.log(subJSonObject);
function myFunction(x) {
  searchLetter=x;
  filterMainObject(searchLetter);
}
function check_subBy_id(word){
	console.log(word)
		var selectedText=[] ;
		  var obj=subJSonObject;
		  
		  for(var i=0;i<obj.length;i++)
		  {
		  	console.log(name);
		    var name=obj[i].parent_id;
		    if(name == word)
		    {
		     selectedText.push(obj[i]);
		    }
		  }
		  if(selectedText.length >= 1){
		  var html='';
				for (var i = 0; i <= selectedText.length-1 ; i++) {
					html += '<option value="'+ selectedText[i].term_id +'">'+ selectedText[i].name +'</option>';
				}
				$('#sub-category').html(html);
				jQuery('#divProgress').hide();
			}else{
				jQuery('#divProgress').hide();
				alert('Sub category is not available');
			}
		  console.log(selectedText);
	}
function filterMainObject(word)
{
  var selectedText=[] ;
  var obj=mainJsonObject;
  for(var i=0;i<obj.length;i++)
  {
    var name=obj[i].name;
    if(compare(name,word))
    {
      selectedText.push(obj[i]);
    }
  }
  console.log(selectedText);
  if(selectedText.length >= 1)
  {
	  jQuery('#divProgress').show();
  	jQuery.post('<?php echo admin_url('admin-ajax.php?lang=' . ICL_LANGUAGE_CODE); ?>', {
				'action': 'auction_model',
				'cat_name': selectedText[0].parent  },function(ress){
					var html='';
						for (var i = 0; i <= selectedText.length-1 ; i++) {
							html += '<option value="'+ selectedText[i].term_id +'">'+ selectedText[i].name +'</option>';
						}
						console.log(localStorage.getItem("id"));
						$('#category-type').html(ress);
					$('#main-category').html(html);
					//$('#main-category').val(localStorage.getItem("id"));
					filterSubObject(selectedText[0].name);
			})
  }else{
  	jQuery('#divProgress').hide();
	alert('Category is not available');
  }
}
function filterSubObject(word)
{
 var selectedText=[] ;
  var obj=subJSonObject;
  for(var i=0;i<obj.length;i++)
  {
    var name=obj[i].parent;
    if(compare(name,word))
    {
     selectedText.push(obj[i]);
    }
  }
  if(selectedText.length >= 1){
  var html='';
		for (var i = 0; i <= selectedText.length-1 ; i++) {
			html += '<option value="'+ selectedText[i].term_id +'">'+ selectedText[i].name +'</option>';
		}
		$('#sub-category').html(html);
		jQuery('#divProgress').hide();
	}else{
		jQuery('#divProgress').hide();
		alert('Sub category is not available');
	}
  console.log(selectedText);
}
function compare(string1,string2)
{
  var str=[string1,string2];
//  console.log(str);
  var stringLength = string2.length;
  if(string2.length>string1.length)
  {
    return false;
  }
  /*if(stringLength>string2.length)



  {



    stringLength=string2.length;



  }*/

  for(var i=0;i<stringLength;i++)
  {
    if(string1[i]==string2[i])
    {
      var charCode1=string1[i].charCodeAt(0);
      var charCode2=string2[i].charCodeAt(0);
      var char=[charCode1,charCode2];
    }
    else 
    {
      var charCode1=string1[i].charCodeAt(0);
      var charCode2=string2[i].charCodeAt(0);
      var char=[charCode1,charCode2];
      //console.log(char);

      if((charCode1-32)!=charCode2&&(charCode1)!=charCode2-32)
      {
        return false;
      }
    }
  }
  return true;
}

	$(document).ready(function(){
var timer;
   jQuery('#query').keyup(function(event)
		 {//alert($(this).val());
		 	clearTimeout(timer);  
		 	var result = $(this).val().split(" ");//clear any running timeout on key up
		 	    timer = setTimeout(function() {
		 	    	if (result[0].length > 2) {
						myFunction(result[0]);
		 	    	}

		 }, 1000);
		 	   console.log(timer);		 	  
		 		//$('#main-category').trigger('change');
		 })
		 })
		jQuery('#MainContent_ddlCategory').change(function(){
			jQuery('#divProgress').show();
		 			var id=$(this).val();

		 			var name = $(this).text();
				 	jQuery.post('<?php echo admin_url('admin-ajax.php?lang=' . ICL_LANGUAGE_CODE); ?>', {
						'action': 'button_value',
						'id': id, 
						 },function(response){
						 	$('#auction').click(function(){
								if($(this).prop('checked')){
									$('.times').removeAttr('disabled','disabled');
								}else{
									
									$('.times').attr('disabled','disabled');
									$('.times').val('');
								}
							})
							var res = JSON.parse(response);
							console.log(response);
							var html=' <option selected="selected" value disabled><?php _e("-- Select --","mazadd"); ?></option>';
							for (var i = 0; i <=  res.length-1 ; i++) {
								html += '<option value="'+ res[i].term_id +'">'+ res[i].name +'</option>';
							}
							$('#MainContent_ddlSubCategory').html(html);
							jQuery('#divProgress').hide();
							jQuery('#MainContent_ddlSubCategory').change(function(){
								jQuery('#divProgress').show();
					 			var id=$(this).val();
					 			var name = $(this).text();
							 	jQuery.post('<?php echo admin_url( 'admin-ajax.php' ); ?>', {
									'action': 'button_value',
									'id': id, 
									 },function(response){
										var res = JSON.parse(response);
										console.log(response);
										var html=' <option selected="selected" value disabled><?php _e("-- Select --","mazadd"); ?></option>';
										for (var i = 0; i <=  res.length-1 ; i++) {
											html += '<option value="'+ res[i].term_id +'">'+ res[i].name +'</option>'
										}
										$('#MainContent_ddlSubSubCategory').html(html);
										jQuery('#divProgress').hide();

								})
							 })
					})
				 })

$('#auction').click(function(){
	if($(this).prop('checked')){
		$('.times').removeAttr('disabled','disabled');
		$('.times').attr('required','required');
	}else{
		
		$('.times').attr('disabled','disabled');
		$('.times').removeAttr('required','required');
		$('.times').val('');
	}
})
</script>

<?php } ?>
<script type="text/javascript">

		function selectCountry(str){



				jQuery('#divProgress').show();	



			 	jQuery.post('<?php echo admin_url( 'admin-ajax.php' ); ?>', {



					'action': 'cities',



					'country': str, 



					 },function(response){



						var res = JSON.parse(response);



						console.log(res);



						var html=' <option selected="selected" value disabled><?php _e("-- Select --","mazadd"); ?></option>';



						for (var i = 0; i <=  res.length-1 ; i++) {



							html += '<option value="'+ res[i].City_Name +'">'+ res[i].City_Name +'</option>'



						}



						$('#cities').html(html);



						jQuery('#divProgress').hide();







		})



			 }



		function selectCountryEstate(str){



				jQuery('#divProgress').show();	



			 	jQuery.post('<?php echo admin_url( 'admin-ajax.php' ); ?>', {



					'action': 'cities',



					'country': str, 



					 },function(response){



						var res = JSON.parse(response);



						console.log(res);



						var html=' <option selected="selected" value disabled><?php _e("-- Select --","mazadd"); ?></option>';



						for (var i = 0; i <=  res.length-1 ; i++) {



							html += '<option value="'+ res[i].City_Name +'">'+ res[i].City_Name +'</option>'



						}



						$('#cities').html(html);







		})



			 	jQuery.post('<?php echo admin_url( 'admin-ajax.php' ); ?>', {



					'action': 'regions',



					'country': str, 



					 },function(response){



						var res = JSON.parse(response);



						console.log(res);



						var html=' <option selected="selected" value disabled><?php _e("-- Select --","mazadd"); ?></option>';



						for (var i = 0; i <=  res.length-1 ; i++) {



							html += '<option value="'+ res[i].Region_Name +'">'+ res[i].Region_Name +'</option>'



						}



						$('#regions').html(html);



						jQuery('#divProgress').hide();







		})



			 }



		 </script>

<script type="text/javascript">
	$('.auction_bid').click(function(){
		/*alert($(this).data('id'));
		alert();*/
		jQuery.post('<?php echo admin_url( 'admin-ajax.php' ); ?>', {
					'action': 'arCart',
					'bid':$('#_actual_bid').val(), 
					'product': $(this).data('id'), 
					 },function(response){
					 	//alert(response);

		})
	})
	
</script>

		



		 



<?php }




Auction_Complete_notification();


add_action( 'wp_footer', 'print_spectro_scripts' );

function custom_loginlogo() {
echo '<style type="text/css">
h1 a {background-image: url('.get_bloginfo('template_directory').'/Images/banner-logo.png) !important; }
</style>';
}
add_action('login_head', 'custom_loginlogo');
function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

add_action( 'wp_ajax_arCart','yith_wcact_add_bid');
add_action( 'wp_ajax_nopriv_arCart','yith_wcact_add_bid');

function yith_wcact_add_bid()
        {
            $userid = get_current_user_id();
           
                $bid = $_POST['bid'];
                $product_id = apply_filters( 'yith_wcact_auction_product_id',$_POST['product'] );
                $date = date("Y-m-d H:i:s");

                
                if($my_current_lang = apply_filters( 'wpml_current_language', NULL ) == 'en' ){ 
                	echo $product_id =apply_filters( 'wpml_object_id', (int)$_POST['product'], 'product', FALSE, 'ar' );
            	}else{
            		echo $product_id =apply_filters( 'wpml_object_id', (int)$_POST['product'], 'product', FALSE, 'en' );
            	}
                $product = wc_get_product($product_id);
                
                if ($product && $product->is_type('auction')) {
                    $bids = YITH_Auctions()->bids;
                    $current_price = $product->get_price();
                    $exist_auctions = $bids->get_max_bid($product_id);
                    $last_bid_user = $bids->get_last_bid_user($userid, $product_id);

                    if ($exist_auctions) {
                        if ($bid > $current_price && !$last_bid_user) {
                            $bids->add_bid($userid, $product_id, $bid, $date);
                        } elseif ($bid > $last_bid_user && $bid > $current_price) {
                            $bids->add_bid($userid, $product_id, $bid, $date);
                        }
                    } else {
                        if ($bid >= $current_price) {
                            $bids->add_bid($userid, $product_id, $bid, $date);
                        }
                    }
                    $user_bid = array(
                        'user_id' => $userid,
                        'product_id' => $product_id,
                        'bid' => $bid,
                        'date' => $date,
                        'url'   =>  get_permalink($product_id),
                    );

                    $actual_price = $product->get_current_bid();
                    yit_save_prop($product,'_price', $actual_price);
             
           
        }
         die();
}
  function add_bid( $user_id, $auction_id, $bid, $date) {
            global $wpdb;

            $insert_query = "INSERT INTO wp_yith_wcact_auction (`user_id`, `auction_id`, `bid`, `date`) VALUES ('" . $user_id . "', '" . $auction_id . "', '" . $bid . "' , '" . $date . "' )";
            $wpdb->query( $insert_query );
        }
/*function mysite_woocommerce_payment_complete( $order_id ) {
    echo  "<script>console.log('Payment has been received for order ".$order_id."')</script>" ;
}
add_action( 'woocommerce_payment_complete', 'mysite_woocommerce_payment_complete', 10, 1 );*/

function my_custom_language_switcher() {
    $languages = apply_filters( 'wpml_active_languages', NULL, array( 'skip_missing' => 0, 'link_empty_to' => 'http://domain.com/missing-translation-contact-form' ) );
 
    if( !empty( $languages ) ) {
        foreach( $languages as $language ){
            $native_name = $language['active'] ? strtoupper( $language['native_name'] ) : $language['native_name'];
 
            if( !$language['active'] ) echo '<a href="' . $language['url'] . '">';
            echo $native_name . ' ';
            if( !$language['active'] ) echo '</a>';
        }
    }
}

function remove_product($c_id,$t_id,$link=''){
	wp_delete_post($c_id, true );
	wp_delete_post( $t_id, true );
	if($link==''){
		$url = home_url();
	}else{
		$url = $link;
	}
	
	echo "<script>window.location = '".$url."';</script>";                
}
function all_media(){
	$query_images_args = array(
	    'post_type'      => 'attachment',
	    'post_mime_type' => 'image',
	    'post_status'    => 'inherit',
	    'posts_per_page' => - 1,
	);

	$query_images = new WP_Query( $query_images_args );
	foreach ( $query_images->posts as $image ) {
	    var_dump($image);
	}
}
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

function new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
  $cols = 10;
  return $cols;
}
