<?php 
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package mazadd
 */
session_start();
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head id="Head1">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="<?php esc_html(bloginfo('description')); ?>">


    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/fav.png">
    <?php wp_head(); ?>
 


 <!-- <link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/alarabiya" > -->
    <style type="text/css">

.navbar-default .navbar-toggle:focus, .navbar-default .navbar-toggle:hover {    background-color: #000 !important;}

li {list-style:none;}



ol, ul {}

header #divWelcomePanel {text-align:center; padding-left:45%;}
		

.adviewtitle1 {margin-bottom:20px;}
		
#lnkLanguage a {
   
    margin-left: 5px;
	margin-top: 10px;
}		
		
		
		#InkLanguage {}		

.bs-docs-footer {margin-left:-20px; margin-right:30px;}

.adviewstyle1 img {margin-bottom:35px; margin-top:35px;}

header #spnPostADTop {margin-top:15px;}

.bs-docs-footer {padding-top:20px !important;}

.attachdetailshome {top:150px;}

.featuredcats li img {margin-bottom:10px;}


.thumbnail {padding:16px;}

h5 {text-decoration:none;}

a {text-decoration:none !important;}

.show {
    display: inline-block !important;
}

.spnbuttonicon {border-radius:4px;}

#containermain
            {
                padding-top: 10px;
                padding-bottom: 5px;
            }      

.featuredcats li img {width:auto; height:auto;}

.fa-search {
    background: #000;
    border: 2.9px solid #000;
    margin-left: -4px;
color:#fff;
line-height:15px;}


/*header #divWelcomePanel .spnNotification {display:none;}*/

header #divWelcomePanel #spnWelcome {margin-top:36px;}
		
		.log   { margin-top: 15px; }

        .navbar-default .navbar-nav > li > a
        {
            font-size: 14px;
        }
        #containermain
        {
            padding-top: 10px;
            padding-bottom: 20px;
        }        
        @media (max-width:620px)
        {
            #containermain
            {
                padding-top: 10px;
                padding-bottom: 5px;
            }     

.my-office {
margin-left: 8px;}

.show {
    display:block !important;
}
			

					
			

        }


.navbar-nav > li {
    float: left;
    border-right:none !important; }

.navbar-toggle:hover {background-color:#000 !important;}


.bs-docs-footer {padding:5px;}

.fa-search {
    background: #000;
    border: 2px solid #000;
    margin-left: -4px;
color:#fff;
height:24px;}


        .navbar-default .navbar-nav > li > a
        {
            font-size: 14px;
        }
        #containermain
        {
            padding-top: 10px;
            padding-bottom: 20px;
        }    


@media (max-width: 991px) {
  .navbar-header {
      float: none;
  }
  .navbar-left,.navbar-right {
      float: none !important;
  }
  .navbar-toggle {
      display: block;
  }
  .navbar-collapse {
      border-top: 1px solid transparent;
      box-shadow: inset 0 1px 0 rgba(255,255,255,0.1);
  }
  .navbar-fixed-top {
      top: 0;
      border-width: 0 0 1px;
  }
  .navbar-collapse.collapse {
      display: none!important;
  }
  .navbar-nav {
      float: none!important;
      margin-top: 7.5px;
  }
  .navbar-nav>li {
      float: none;
  }
  .navbar-nav>li>a {
      padding-top: 10px;
      padding-bottom: 10px;
  }
  .collapse.in{
      display:block !important;
  }
  
  .spnbuttonicon {
    display: block;
    width: 100%;
text-align:center;
}

.show {margin-top:16px; width:100%; text-align:center;}

.bs-docs-footer {margin-left: -20px;
    margin-right: 0px;}

  
  .thumbnailslider {width:29%;}
  
  header nav #spnsearchkeywordexpnd {margin-bottom:20px; margin-left:10px; float:left;}
  
  
  .searchad-blk {margin-left:-10px; padding-left:4px !important;}

.searchad-blk-two{  padding-left: 10px !important;
  }
  
  #lnkLanguage {    margin-right: 20px;
   }
  
  
	header #divWelcomePanel {
    text-align: center;
    padding-left: 0%;
}
  
  
  }




@media (max-width:950px) {


header #divWelcomePanel {padding-left:0%;}
	
	


}





@media screen and (max-width: 782px) {
html {
    margin-top: 0px !important;
}
}


@media (max-width:768px) {

header nav #spnsearchkeywordexpnd {margin-bottom:20px; margin-left:0px; float:left;}

/* #lnkLanguage {    margin-right: 15px !important;
    float: right;} */

.thumb {min-height:330px !important;}

.bs-docs-footer {
        margin-right: -36px;}

header #divWelcomePanel {padding-left:0%;}

.thumb {margin-left:0%;}



.spnbuttonicon {
    display: block;
    width: 100%;
}

.show {margin-top:16px; width:100%; }


}



@media (max-width:720px) {

header #divWelcomePanel {padding-left:0%;}

}

		
		
		@media (min-width:666px) and (max-width:710px)  {
			
			
			.log {margin-top: 0px;}	
			
			header #divWelcomePanel #spnWelcome {}
			
		}
		
		
		
		 @media (max-width:665px)
        {
			.log {margin-top:;}	
			
			header #divWelcomePanel #spnWelcome {}
			
		}
		
		
			

        @media (max-width:620px)
        {
            button, input, select, textarea
            {
                font-size: 12px;
            }

.thumb {min-height:0px !important;}
			
			
		
			
			header #divWelcomePanel #spnWelcome {margin-right:0px; top:10px;}


.attachdetailshome {
    margin-left: 0px;
}
.lostitems {padding-left: 16px !important; margin-left:0px !important;}

.panel-group .panel {
    margin-right: 0px !important;
}

.panel-default > .panel-heading {
    background: #000 !important;
}

.bs-docs-footer {
    margin-right: -30px !important;}



header nav #spnsearchkeywordcoll {
    display: block;
margin-right: 15px;}

.fa-search:before {color:#fff;}

.fa-search {background:#000; border:2px solid #000;}

.fa-search {
    background: #000;
    border: 2px solid #000;
    margin-left: -4px;
line-height:16px;
color:#fff;}


header #divWelcomePanel {
    padding-left: 0%;
}

header #spnPostADTop {
float: right; margin-top:0px; }

header #divWelcomePanel .spnNotification {margin-left:0px;}

        }





            .carousel
            {
                padding: 0px;
                padding-bottom: 10px;
            }
            .carousel-control
            {
                display: none;
            }
            .well
            {
                padding: 3px;
            }
            .carousel-indicators
            {
                bottom: 0px !important;
				
            }
            .menuTabs
            {
                position: relative;
                top: 1px;
                left: 10px;
                font-size: 12px;
            }
            #MainContent_menuTabs a.static.selected
            {
                padding: 5px 4px;
            }
            #MainContent_menuTabs a.static
            {
                padding: 5px 4px;
            }
            .chzn-container
            {
                width: 100%;
            }
        
.carousel-inner {margin-top:20px;}


@media (min-width:580px) and (max-width:620px){

/* #lnkLanguage {    margin-right: -10px; text-align:right;
    float: right;} */

header #spnPostADTop {margin-left:180px;}

.thumb {margin-left:0%;}

		.log {
        margin-top: 10px !important;
}
			
			header #divWelcomePanel #spnWelcome {margin-right: 160px;
    margin-top: 65px;}
	

.attachdetailshome {
    margin-left: 15px;
}

}




@media (max-width:580px) {






}





@media (min-width:481px) and (max-width:579px) {


header #spnPostADTop {margin-left:120px;}

/* #lnkLanguage { margin-left:0px;   margin-right: -10px; text-align:right;
    float: right;} */

.log {
        margin-top:0px !important;
}
			
			header #divWelcomePanel #spnWelcome {margin-right: 120px;
    margin-top: 65px;}


.thumb {margin-left:0%;}


.attachdetailshome {
    margin-left: 0px !important;
}


}











@media (min-width:333px) and (max-width:480px) {


/* #lnkLanguage {margin-left: -10px; margin-right: -10px; text-align:right;
    float: right;} */
	
.log {
        margin-top: 21px !important;
}
			
			header #divWelcomePanel #spnWelcome {margin-right:80px; margin-top: 66px;}	

header #spnPostADTop {margin-left:115px;}
.thumb {margin-left:0%;}



.attachdetailshome {
    margin-left: 0px !important;
}


}





@media (min-width:400px) and (max-width:470px) {


.attachdetailshome {
    margin-left: 10px;
}

header #spnPostADTop {margin-left:10px;}
	
/* #lnkLanguage {margin-right: -10px;
    } */	


}



@media (min-width:335px) and (max-width:420px) {

header #spnPostADTop {
    margin-left: 60px; margin-top:-60px;
}

	
	.log {
        margin-top: 21px !important;
}
			
header #divWelcomePanel #spnWelcome {margin-top: 66px; margin-right: 80px;}	
	
	
/* #lnkLanguage {margin-right: -10px; 
    } */	


.attachdetailshome {
    margin-left: 220px;
}


}



@media (min-width:334px) and (max-width:400px) {


header #spnPostADTop {margin-left:45px; margin-top:-60px;}

/* #lnkLanguage { margin-right: -70px; margin-left: -30px; text-align:center !important;} */

.thumb {margin-left:0% !important;}
	
/* #lnkLanguage {margin-right: -10px; 
    } */		


	.log {
       margin-top: 21px !important;
}
			
			header #divWelcomePanel #spnWelcome {margin-right: 70px;
    margin-top: 66px;
}

.attachdetailshome {
    margin-left: 155px;
}




}






@media (min-width:321px) and (max-width:333px) {



/* #lnkLanguage { margin-left: 0px !important; margin-right: -10px !important; margin-top: 0px; text-align:center;} */

header #suggestionbox {margin-left:60px;}
	
/* #lnkLanguage {margin-right: -10px; 
    }	*/	

header #spnPostADTop {margin-left:100px; margin-top:-60px;}

	
	.log {
        margin-top: 21px !important;
}
			
			header #divWelcomePanel #spnWelcome {margin-right:65px; margin-top: 66px;}


adposteddetailshome {

    margin-left: 20px !important;
}


.attachdetailshome {
    margin-left: 155px;
}

.thumb {margin-left:-6% !important;}





}





@media (max-width:320px) {


.addetails {width:100%;}

.thumb {margin-left:0% !important;}

	
/* #lnkLanguage {margin-right: -10px; 
    } */		

header #spnPostADTop {margin-top:-60px;}

adposteddetailshome {

    margin-left: 20px !important;
}
	
	
	.log {
       margin-top: 21px !important;
}
			
			header #divWelcomePanel #spnWelcome {margin-right:50px; margin-top: 66px;}

/* #lnkLanguage { margin-left: -30px; text-align:center !important;} */

header nav #spnsearchkeywordcoll {margin-right:13px;}

header nav #spnsearchkeywordcoll input[type=text], input[type=password] {width:150px;}





adposteddetailshome {

    margin-left: 20px !important;
}





}





    </style>
  
</head>
<body style="background-color: #fff; ">
    
    <div class="header" style="display: none;">
        <div id="topmenu">
            
        </div>
        <a id="lnkRegisterTop" class="registertop" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$lnkRegisterTop&quot;, &quot;&quot;, false, &quot;&quot;, &quot;UserRegister.aspx&quot;, false, true))">التسجيل</a>
        <a id="btnSignin" class="signin" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$btnSignin&quot;, &quot;&quot;, false, &quot;&quot;, &quot;Login.aspx&quot;, false, true))">الدخول</a>
        <div class="clear hideSkiplink">
        </div>
    </div>
    <header>
    <div class="container" id="containermain" style="">
            <div class="title">
              <?php if($my_current_lang = apply_filters( 'wpml_current_language', NULL ) == 'en' ){ ?>
                <a href="<?php echo site_url(); ?>" style="text-decoration: none;">
                    <img id="logo" src="<?php echo get_template_directory_uri(); ?>/Images\Logo_Monadie.png" title="mazadd.com" alt="mazadd.com" border="0" style="max-width: 100%;">
                    </a>
                    <?php }else{
                      ?>
                      <a href="<?php echo site_url(); ?>/ar/" style="text-decoration: none;">
                    <img id="logo" src="<?php echo get_template_directory_uri(); ?>/Images\Logo_Monadie.png" title="mazadd.com" alt="mazadd.com" border="0" style="max-width: 100%;">
                    </a>
                      <?php 
                    } ?>
                <br><span id="spnFreeInfo"><?php _e('Buy & Sell Goods without commission','mazadd'); ?></span>
            </div>
             <span id="lnkLanguage">
                <!-- <a id="lnkArabic" title="عربى" href="javascript:__doPostBack(&#39;ctl00$lnkArabic&#39;,&#39;&#39;)">عربى <img src='Images\saudi_flag.png' width='20' height='14'></a><br> -->  
<!--<img src="Images\on.png" id="imgLoginTop" class="logintop" alt="Login" title="Login" onclick="window.location.href=&quot;Login.aspx&quot;">-->



  <!--<label class="toggleSwitch large" onclick="">
        <input type="checkbox" checked />
        <span>
            <span>OFF</span>
            <span>ON</span>
     
        </span>
        <a></a>
    </label>-->
     
       <?php
     my_custom_language_switcher();
     if (is_user_logged_in()) {
      ?>
      <?php if($my_current_lang = apply_filters( 'wpml_current_language', NULL ) == 'en' ){ ?>
      <div class="log">
	
		 <span id="spnNotification" class="spnNotification">
          
                <a id="ads" title="New Comments" href="<?php echo site_url(); ?>/my-account/messages/"><img src="<?php echo get_template_directory_uri(); ?>/Images/message.png"/></a><i style="background-color: black;
    color:#fff;
    font-size: 12px;
    font-weight: normal;
	margin-top: -10px;
	margin-right: 10px;
    padding: 1px 3px;
    border-radius: 4px;"><?php echo get_all_notifications(); ?></i>
                <!-- <a id="contracts" title="Contracts" href="#"><img src="Images/shake.png"/></a><i>0</i> -->

                   <!-- <div id="divContractNotification">
                        <ul>
                            <li><a style="color: #4091D2; font-size: 11px; font-weight: bold;" href="EmployeeContractInbox.aspx">
                                New Contracts(0)</a></li><li><a style="color: #4091D2; font-size: 11px;
                                    font-weight: bold;" href="EmployeeContractResign.aspx?tid=2">Resigned Contracts(0)</a></li><li>
                                        <a style="color: #4091D2; font-size: 11px; font-weight: bold;" href="EmployeeContractVacation.aspx?tid=2">
                                            Vacation Contracts(0)</a></li></ul>
                    </div> -->
                    <a id="auctions" title="New Auctions" href="<?php echo site_url(); ?>/shop/"><img src="<?php echo get_template_directory_uri(); ?>/Images/raise.png"/></a><i class="auc" style="background-color: black;
    color:#fff;
    font-size: 12px;
    font-weight: normal;
	margin-top: -10px;
	margin-right: 10px;
    padding: 1px 3px;
    border-radius: 4px;
	">
                        0</i> 
			 
			 
       <a href="<?php echo wp_logout_url( site_url().'/login/?l=my-account' ); ?>" class="logout" id="logout"><img src="<?php echo get_template_directory_uri(); ?>/Images/login.png"/><small> <?php _e('Logout','mazadd'); ?> </small></a>
       
       <?php }else{ ?>
       <div class="log">
  
     <span id="spnNotification" class="spnNotification">
          
                <a id="ads" title="<?php _e('New Comments','mazadd'); ?>" href="<?php echo site_url(); ?>/ar/حسابي/messages/"><img src="<?php echo get_template_directory_uri(); ?>/Images/message.png"/></a><i style="background-color: black;
    color:#fff;
    font-size: 12px;
    font-weight: normal;
  margin-top: -10px;
  margin-right: 10px;
    padding: 1px 3px;
    border-radius: 4px;"><?php echo get_all_notifications(); ?></i>
                <!-- <a id="contracts" title="Contracts" href="#"><img src="Images/shake.png"/></a><i>0</i> -->

                   <!-- <div id="divContractNotification">
                        <ul>
                            <li><a style="color: #4091D2; font-size: 11px; font-weight: bold;" href="EmployeeContractInbox.aspx">
                                New Contracts(0)</a></li><li><a style="color: #4091D2; font-size: 11px;
                                    font-weight: bold;" href="EmployeeContractResign.aspx?tid=2">Resigned Contracts(0)</a></li><li>
                                        <a style="color: #4091D2; font-size: 11px; font-weight: bold;" href="EmployeeContractVacation.aspx?tid=2">
                                            Vacation Contracts(0)</a></li></ul>
                    </div> -->
                    <a id="auctions" title="<?php _e('New Auctions','mazadd'); ?>" href="<?php echo site_url(); ?>/ar/shop/"><img src="<?php echo get_template_directory_uri(); ?>/Images/raise.png"/></a><i class="auc" style="background-color: black;
    color:#fff;
    font-size: 12px;
    font-weight: normal;
  margin-top: -10px;
  margin-right: 10px;
    padding: 1px 3px;
    border-radius: 4px;
  ">
                        0</i>
			 <a href="<?php echo wp_logout_url( site_url().'/ar/تسجيل-الدخول/?l=حسابي' ); ?>" class="logout" id="logout"><img src="<?php echo get_template_directory_uri(); ?>/Images/login.png"/><small> <?php _e('Logout','mazadd'); ?> </small></a>
       <?php }?>
                        </span> 
		  
      <div id="divWelcomePanel" class="container"> <span id="spnWelcome">
            <span id="lblWelcome"><?php _e('Welcome','mazadd'); ?></span>&nbsp; <br>
            <span id="lblUserName" style="color:Red; font-size:11px;"> <?php echo $current_user = wp_get_current_user()->user_login;?></span>
            &nbsp;&nbsp;           
            
                
                
        </span>
        
        
        </div>
        
        
     </div>
      <?php

        wp_get_current_user();
     }else{
?>
 <div class="log">
   <?php if($my_current_lang = apply_filters( 'wpml_current_language', NULL ) == 'en' ){ ?>
<a href="<?php echo site_url(); ?>/login" id="login"><img src="<?php echo get_template_directory_uri(); ?>/Images/login.png"/> <small> <?php _e('Login','mazadd'); ?></small></a>
<?php }else{ ?>
  <a href="<?php echo site_url(); ?>/ar/تسجيل-الدخول/" id="login"><img src="<?php echo get_template_directory_uri(); ?>/Images/login.png"/> <small> <?php _e('Login','mazadd'); ?></small></a>
<?php }?>
 </div>
<?php
     } 
     ?>
				
   
  <!-- Username & details -->
  
  <!-- <div id="divWelcomePanel" class="container"> <span id="spnWelcome">
            <span id="lblWelcome">Welcome</span>&nbsp;
            <span id="lblUserName" style="color:Red;">akhilaki.sg@gmail.com</span>
            &nbsp;&nbsp;           
            
                
                
        </span>
         <span id="spnNotification" class="spnNotification"><a id="msgs" title="New Messages" href="#"><img src="Images/message.png"></a><i>
                        <span id="lblMsgNotification">0</span></i><a id="ads" title="New Comments" href="#"><img src="Images/speaker.png"></a><i>0</i><a id="contracts" title="Contracts" href="#"><img src="Images/shake.png"></a><i>0</i>
                    <div id="divContractNotification">
                        <ul>
                            <li><a style="color: #4091D2; font-size: 11px; font-weight: bold;" href="EmployeeContractInbox.aspx">
                                New Contracts(0)</a></li><li><a style="color: #4091D2; font-size: 11px;
                                    font-weight: bold;" href="EmployeeContractResign.aspx?tid=2">Resigned Contracts(0)</a></li><li>
                                        <a style="color: #4091D2; font-size: 11px; font-weight: bold;" href="EmployeeContractVacation.aspx?tid=2">
                                            Vacation Contracts(0)</a></li></ul>
                    </div>
                    <a id="auctions" title="New Auctions" href="#"><img src="Images/raise.png"></a><i>
                        0</i> </span>
        
        </div> -->
        
  
  
 
				 
				 
				 <?php
     if (is_user_logged_in()) {

        wp_get_current_user();
     }else{

     } 
     ?>
    </div>
   

<div class="row">



</div>
                    </span>
            
                        <!-- <span id="suggestionbox" title="Post your suggestions" border="0" onclick="window.location.href='suggestionBox.html';">Post Suggestions</span> -->
                       <!-- <span id="spnPostADTop" title="Post AD" border="0" onclick="window.location.href='<?php echo site_url(); ?>/ad-post/'">Post AD</span>-->
		
		
		
		
		
		
		
           
        </div>
        <div class="bs-docs-nav navbar navbar-static-top" id="top">
    
    <div class="container">
    <nav class="navbar navbar-default" style="display: block;">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
       <span id="spnsearchkeywordcoll"><input name="ctl00$txtSearchKeywordColl" type="text" id="txtSearchKeywordColl">
      <button type="button" class="fa fa-search" onclick="searchkeyword();"></button>
      </span>
      <a class="navbar-brand" href="#"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

       <?php
        if (is_user_logged_in()) {
wp_nav_menu( array(

'menu_id'        => 'primary-menu',
'menu_class'        => 'nav navbar-nav',
                    'theme_location' => 'menu-1',
                    'container' => false,
                    'walker'=> new wp_bootstrap_navwalker
) );
            }else{
                wp_nav_menu( array(
                    
                    'menu_id'        => 'primary-menu',
                    'menu_class'        => 'nav navbar-nav',
                    'theme_location' => 'menu-2',
                    'container' => false,
                    'walker'=> new wp_bootstrap_navwalker
                ) );
            }
?>
<?php if($my_current_lang = apply_filters( 'wpml_current_language', NULL ) == 'en' ){ 
        $site_url = site_url();
      }else{
        $site_url = site_url().'/ar/';
      }
?>
      <span id="spnsearchkeywordexpnd"><form role="search" method="get" class="woocommerce-product-search" action="<?php echo esc_url( $site_url ); ?>">
    <label class="screen-reader-text" for="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>"><?php _e( '', 'woocommerce' ); ?></label>
    <input type="search" id="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>" class="search-field" placeholder="<?php echo esc_attr__( 'Search products&hellip;', 'woocommerce' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
    <button type="submit" value="<?php echo esc_attr_x( 'Search', 'submit button', 'woocommerce' ); ?>" class="fa fa-search"></button>
    <input type="hidden" name="post_type" value="product" />
</form>
      </span>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
     </div>
			
			
			 

     
</header>
