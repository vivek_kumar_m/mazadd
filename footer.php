<?php
/** * The template for displaying the footer * * Contains the closing of the #content div and all content after. * * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials * * @package mazadd */?>
<div class="container">
	<footer class="bs-docs-footer">         
		<div class="container">             
			<?php   wp_nav_menu( array(                    
				'menu_class'=>'bs-docs-footer-links',                    

				'theme_location' => 'Footer_left_menu',                     
				'container' => false,)                    ); 
				?>        
				 <!--    <ul class="bs-docs-footer-links"> <li><a id="HyperLink1" href="Home.html">Home</a></li>         <li><a id="HyperLink4" href="#">Contact Us</a></li> <li><a id="HyperLink5" href="Login.html">Login</a></li>         <li><a id="HyperLink2" href="Admin\Login.html">Admin</a> </li>        <li><a id="HyperLink6" href="#">About Us</a></li>        <li><a id="HyperLink7" href="#">Mission</a> </li>        <li><a id="HyperLink8" href="#">Vision</a> </li>        <li><a id="HyperLink9" href="#">Privacy Policy</a>  </li>         </ul>  -->        
				 <p>mazadd.com ©2018. <?php _e('All Rights Reserved','mazadd'); ?>  </p>       
				   <p><ul class="bs-docs-footer-links-social">
				   	<li><a target="_blank" href="#" class="" title="visit our page on twitter "><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>                        <li><a target="_blank" href="#" class="" title="visit our page on facebook "><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>                        <li><a target="_blank" href="#" class="" title="visit our page on linkedin"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>  


				   	  </ul>                       
				   	   </p>        
				   	</div> 
				   </footer>        
				</div>      
				<?php wp_footer(); ?>
				<script type="text/javascript">        
				$(document).ready(function () {            
				$('#myCarousel').carousel({                
				interval: 3000            
			})       
			 });    
			</script>    
			<script>$('.nav li').click(function() {    
			$(this).siblings('li').removeClass('active');    
		$(this).addClass('active');
	});
	function ToggleDiv(ctrldiv) {           
	 $('#' + ctrldiv).slideToggle();        
	}
$('#myTabs a').click(function (e) { 
 e.preventDefault();  
$(this).tab('show')})
</script></body></html>