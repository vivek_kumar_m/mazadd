<?php




/**




 * The template for displaying product content in the single-product.php template




 *




 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.




 *




 * HOWEVER, on occasion WooCommerce will need to update template files and you




 * (the theme developer) will need to copy the new files to your theme to




 * maintain compatibility. We try to do this as little as possible, but it does




 * happen. When this occurs the version of the template file will be bumped and




 * the readme will list any important changes.




 *




 * @see     https://docs.woocommerce.com/document/template-structure/




 * @author WooThemes




 * @package WooCommerce/Templates




 * @version     3.0.0




 */









if ( ! defined( 'ABSPATH' ) ) {




exit; // Exit if accessed directly




}









?>









<?php

/*remove_product($c_id,$t_id)*/


global $product;

if(isset($_REQUEST['remove_id'])){
  $c_id =apply_filters( 'wpml_object_id', (int)$product->get_id(), 'product', FALSE, 'ar' );
  $t_id =apply_filters( 'wpml_object_id', (int)$product->get_id(), 'product', FALSE, 'en' );
  remove_product($c_id,$t_id);
}

    



/**




 * woocommerce_before_single_product hook.




 *




 * @hooked wc_print_notices - 10




 */




 do_action( 'woocommerce_before_single_product' );









 if ( post_password_required() ) {




 echo get_the_password_form();




 return;




 }




?>




<style type="text/css">


@media (max-width:620px) {
	
	
	
.bs-docs-footer {
    margin-right: -30px !important;}	
	
	
}


.col-sm-9 {




    padding: 0px;




}




.col-sm-12 {




    padding: 0px;




}




</style>




<div class="container bs-docs-container">




        <div class="main">




<div id="bodycont" <?php post_class(); ?> >




        <div class="row">




            <div class="col-sm-12">




                <div class="col-sm-12">




                




            




      <div class="divTable">




      <div class="adviewtitle1">




      <?php do_action( 'breadcrumb_sections' );?>




      </div>




      




      <?php do_action( 'after_breadcrumb_sections' );?>




      




       <div class="adviewstyle2">




       









        <span id="spnMobile" class="spnbuttonicon"><i class="fa fa-phone-square" style="font-size: 20px;




                                                            color: White;"></i>&nbsp;<?php echo get_post_meta( $product->get_id(),'_auction_phone',true); ?></span>&nbsp;

                                                          


         



         <span id="Span1" class="spnbuttonicon"><i class="fa fa-envelope" style="color:white;"></i>&nbsp;<a href="" class="aspNetDisabled btnsendmsg" style="color: White;"> <?php _e('Private Message','mazadd');?> </a> </span>




                                            &nbsp;




          <?php  if(is_product_closed( $product->get_id())){
          if(get_the_author_meta('ID') == get_current_user_id()){ 

          ?>  
          <span id="Span1" class="spnbuttonicon"><a href="?remove_id=<?php echo $product->get_id(); ?>"  class="" style="color:white;"><?php _e('remove','mazadd'); ?></a></span>&nbsp;

          <?php if($my_current_lang = apply_filters( 'wpml_current_language', NULL ) == 'en' ){ ?>
          

                    <a href="<?php echo site_url(); ?>/update_ad?pos_id=<?php echo $product->get_id(); ?>"  class="spnbuttonicon"><?php _e('auction','mazadd'); ?></a>

          <?php }else{ ?>

          		<a href="<?php echo site_url(); ?>/ar/تحديث-الإعلان?pos_id=<?php echo $product->get_id(); ?>"  class="spnbuttonicon"><?php _e('auction','mazadd'); ?></a>

           <?php }  
              }     
           }

           if (is_user_logged_in()) {
            ?>             




           <?php echo do_shortcode('[yith_wcwl_add_to_wishlist]'); 
         }

         ?>             




          <!--&nbsp;<span id="Span1" class="spnbuttonicon"><i class="fa fa-skype" style="font-size:20px;color:White;"></i>&nbsp;




                                            <input type="submit" name="ctl00$MainContent$btnSkypeCall" value="Skype" id="MainContent_btnSkypeCall" disabled="disabled" class="aspNetDisabled btnskypecall" /></span>-->




                                        </div>



       




       




       




                                        <div id="MainContent_divImages" class="adviewstyle1" style="margin-top: 20px;">       




<?php




/**




 * woocommerce_before_single_product_summary hook.




 *




 * @hooked woocommerce_show_product_sale_flash - 10




 * @hooked woocommerce_show_product_images - 20




 */




do_action( 'woocommerce_before_single_product_summary' );









?>




</div>




<div class="summary entry-summary">









<?php

wc_get_template( 'frontend/list-bids.php');


/**




 * woocommerce_single_product_summary hook.




 *




 * @hooked woocommerce_template_single_title - 5




 * @hooked woocommerce_template_single_rating - 10




 * @hooked woocommerce_template_single_price - 10




 * @hooked woocommerce_template_single_excerpt - 20




 * @hooked woocommerce_template_single_add_to_cart - 30




 * @hooked woocommerce_template_single_meta - 40




 * @hooked woocommerce_template_single_sharing - 50




 * @hooked WC_Structured_Data::generate_product_data() - 60




 */




//do_action( 'woocommerce_single_product_summary' );




?>




</div>









</div>




</div>




<div class="col-sm-12"><!-- .summary -->









<?php
/**
* woocommerce_after_single_product_summary hook.
 *
 * @hooked woocommerce_output_product_data_tabs - 10




 * @hooked woocommerce_upsell_display - 15




 * @hooked woocommerce_output_related_products - 20




 */




do_action( 'woocommerce_after_single_product_summary' );




?>









</div>




</div>




</div><!-- #product-<?php the_ID(); ?> -->









<?php do_action( 'woocommerce_after_single_product' ); ?>




