<?php

/**

 * View Order

 *

 * Shows the details of a particular order on the account page.

 *

 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/messages.php.

 *

 * HOWEVER, on occasion WooCommerce will need to update template files and you

 * (the theme developer) will need to copy the new files to your theme to

 * maintain compatibility. We try to do this as little as possible, but it does

 * happen. When this occurs the version of the template file will be bumped and

 * the readme will list any important changes.

 *

 * @see     https://docs.woocommerce.com/document/template-structure/

 * @author  WooThemes

 * @package WooCommerce/Templates

 * @version 3.0.0

 */



if ( ! defined( 'ABSPATH' ) ) {

	exit;

}

$notifications = notification_posts();

$vnotifications = viewed_notification_posts();

$messages = messages_posts();

$rmessages = readed_messages_posts();

?>
<style>
	@media (max-width: 620px) {

.my-office {

    margin-left: 0px !important;
}	

	}	
	.notification {border:1px solid #ccc; padding: 30px; text-align:center; font-size: 18px; margin-bottom:30px;}
	
	.message {border:1px solid #ccc; padding: 30px; text-align:center; font-size: 18px; margin-bottom:30px;}
	
	.my-office-block img {
    border: 1px solid #ed2731;
    text-align: center;
    border-radius: 50%;
    padding: 25px;
    margin-left: 20px;
}




.my-office-block {
    padding-left: 180px;
    margin-top: 50px;
}
	
	
	@media (max-width:991px) {
		
		.my-office-block {padding-left: 280px !important;}
		
	}	
	
	@media (max-width:768px) {
		
		.my-office-block {padding-left: 240px !important;}
		
	}	
	
	
	@media (max-width: 620px) {

.my-office-block {
    padding-left: 200px !important;
}
	
	}
	
	
	@media (max-width: 480px) {

.my-office-block {
    padding-left: 120px !important;
}
	
	}
	
	@media (max-width: 375px) {

.my-office-block {
    padding-left: 100px !important;
}
	
	}
	
	
	@media (max-width: 360px) {

.my-office-block {
    padding-left: 93px !important;
}
	
	}
	

</style>


<div class="container">
<ul class="nav nav-pills nav-stacked col-md-2" id="myTabs">
  <li class="active"><a href="#tab_a" data-toggle="pill"><?php echo _e('Message','mazadd'); ?> <span class="badge" style="background-color: lightgreen"><?php echo count($messages); ?></span></a></li>
  <li><a href="#tab_b" data-toggle="pill"><?php echo _e('Notification','mazadd'); ?> <span class="badge" style="background-color: lightgreen"><?php echo count($notifications); ?></span></a></li>
  <li><a href="#tab_c" data-toggle="pill"><?php echo _e('Compose','mazadd'); ?></a></li>
</ul>
<div class="tab-content col-md-10">
        <div class="tab-pane active" id="tab_a">
            <ul class="listrap">
            
	<?php foreach ($messages as $value) {?>

		<li class="active">
			<a href="<?php echo get_permalink($value->ID); ?>">
                <div class="listrap-toggle">
                	<img src="<?php echo get_template_directory_uri(); ?>/Images/envelope.png"  />
                </div>
			

				<strong><?php echo $value->post_title; ?> </strong>
				<date style="float: right;"><?php echo get_the_date( 'Y-M-d', $value->ID ); ?></date>
				<p style="margin-left: 50px; margin-top:-45px;"> <?php echo $value->post_excerpt; ?> </p>
			</a>

		</li>

	<?php } ?>	

	<?php foreach ($rmessages as $value) {?>

		<li>
			<a href="<?php echo get_permalink($value->ID); ?>">
                <div class="listrap-toggle">
                	<img src="<?php echo get_template_directory_uri(); ?>/Images/mesg.png"/>
                </div>
			

				<strong>  <?php echo $value->post_title; ?> </strong><date style="float: right;"><?php echo get_the_date( 'Y-M-d', $value->ID ); ?></date>
				<p style="margin-left: 50px; margin-top:-45px;"> <?php echo $value->post_excerpt; ?> </p>
			</a>

		</li>

	<?php } ?>
			
			
            </ul>
           
           
        </div>
        <div class="tab-pane" id="tab_b">
             
			 <ul class="listrap">
            
            <?php foreach ($notifications as $value) {?>

		<li class="active">
			<a href="<?php echo get_permalink($value->ID); ?>">
                <div class="listrap-toggle">
                	<img src="<?php echo get_template_directory_uri(); ?>/Images/tick.png" class="img-circle" />
                </div>
			

				<strong><?php echo $value->post_title; ?> </strong>
				<p style="margin-left: 80px; margin-top:-20px;"> <?php echo $value->post_excerpt; ?> </p>
			</a>

		</li>

	<?php } ?>

	<?php foreach ($vnotifications as $value) {?>

		<li>
			<a href="<?php echo get_permalink($value->ID); ?>">
                <div class="listrap-toggle">
                	<img src="<?php echo get_template_directory_uri(); ?>/Images/tick.png" class="img-circle" />
                </div>
			

				<strong><?php echo $value->post_title; ?> </strong>
				<p style="margin-left: 80px; margin-top:-20px;"> <?php echo $value->post_excerpt; ?> </p>
			</a>

		</li>

	<?php } ?>	
            </ul>
			 
        </div>
        <div class="tab-pane" id="tab_c">
             
			<form style="margin-top:20px;" method="post">
		  <div class="form-group">
		    <label for="exampleFormControlInput1"><?php echo _e('To','mazadd'); ?></label>
		    <select class="form-control" name="chats">
		    <option value selected disabled><?php echo _e('--select user--','mazadd'); ?></option>
		    <?php $blogusers = get_users( array( 'fields' => array( 'display_name' ) ) );
		    foreach ($blogusers as $value) {?>	
		    <?php $user = get_userdata( $value->user_id ); ?> 
		    <option value="<?php echo $value->user_id ?>"><?php echo $value->display_name;?></option>
		    <?php } ?>
		    </select>
		  </div>
		  
		  <div class="form-group">
		    <label for="exampleFormControlTextarea1"><?php echo _e('Subject','mazadd'); ?></label>
		    <textarea class="form-control" name="message_title" cols="2" rows="1" required></textarea>
		  </div>
		  
		  <div class="form-group">
		    <label for="exampleFormControlTextarea1"><?php echo _e('Message','mazadd'); ?></label>
		    <textarea class="form-control" name="message_content" rows="3" required=""></textarea>
		  </div>
		  
		  <div class="form-group">
		  	<input type="submit" name="send_message" class="form-control" value="<?php _e('Send','mazadd') ?>" style="width: 20%">
		  </div>
		  
		  
		</form>
<?php





if (isset($_POST['send_message'])) {





	$message_title = $_POST['message_title'];





	$post_content = $_POST['message_content'];





	$id = $_POST['chats'];





	send_messages($id,$message_title,$post_content);





}?>
        </div>
        
</div><!-- tab content -->
</div>





