<?php

/**

 * My Account page

 *

 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.

 *

 * HOWEVER, on occasion WooCommerce will need to update template files and you

 * (the theme developer) will need to copy the new files to your theme to

 * maintain compatibility. We try to do this as little as possible, but it does

 * happen. When this occurs the version of the template file will be bumped and

 * the readme will list any important changes.

 *

 * @see     https://docs.woocommerce.com/document/template-structure/

 * @author  WooThemes

 * @package WooCommerce/Templates

 * @version 2.6.0

 */



if ( ! defined( 'ABSPATH' ) ) {

	exit;

}



wc_print_notices();



/**

 * My Account navigation.

 * @since 2.6.0

 */

?>

<style type="text/css">

	

	

	

.my-office-sec img {

    border: 1px solid #ed2731;

    text-align: center;

    border-radius: 50%;

    padding: 25px;

    margin-left: 20px;

}



.my-office-sec {

    padding-left: 55px;

    margin-top: 30px;

	text-decoration:none;

}

	

	

	.my-office-sec h5 {text-decoration:none !important;}	


	@media (min-width: 1200px) {

.container {
    width: 970px;
}

	}


@media (max-width: 1200px) {



.my-office-sec {

    padding-left: 65px !important;

}
	
	
	
	
	




}







@media (max-width: 991px) {

.my-office-sec {

    padding-left: 35px !important;

}





 #lnkLanguage {    margin-right: 20px;

   }



}





@media (max-width: 768px) {



.my-office-sec {

    margin-left: 235px;

}



.bs-docs-footer {

   margin-right: 0px !important; 
    margin-left: 0px;

}



#lnkLanguage {    margin-right: 15px;

    float: right;}



}



@media screen and (max-width: 750px) {

.my-office-sec {

    margin-left: 220px;

}



}







@media (max-width: 640px) {

.my-office-sec {

    margin-left: 130px !important;

}



}





@media (max-width: 620px) {



.my-office {

    margin-left: 0px;

}



.my-office-sec {

    margin-left: 170px !important;

}

	

	.bs-docs-footer {

    margin-left: 0px;

}



}







@media (min-width:580px) and (max-width:620px)	{

	

	#lnkLanguage {    margin-right: 15px;

    float: right;}

	

	header #suggestionbox {margin-left:180px;}

	

	.thumb {margin-left:0%;}

	

	.attachdetailshome {

    margin-left: 220px;

}

	

	}







@media (min-width:481px) and (max-width:579px) {





header #suggestionbox {margin-left:120px;}



#lnkLanguage {   margin-right: 15px;

    float: right;}



	

.thumb {margin-left:0%;}





.attachdetailshome {

    margin-left: 230px;

}	





.my-office-sec {

    margin-left: 150px !important;

}

	

	

}









@media (max-width: 576px) {





}



@media (max-width: 480px) {

.my-office-sec {

    margin-left: 120px !important;

}



}







@media (min-width:333px) and (max-width:480px) {

		

		

	#lnkLanguage { 

    float: right;}

		

		header #suggestionbox {margin-left:115px;}		

		.thumb {margin-left:0%;}

		

		.attachdetailshome {

    margin-left: 180px;

}

		

		

		}

		





@media (min-width:400px) and (max-width:470px) {

		

		

			.attachdetailshome {

    margin-left: 180px;

}

		

			header #suggestionbox {margin-left:90px;}

		

		

		}









@media (max-width: 450px) {

.my-office-sec {

    margin-left: 90px !important;

}



}







@media (min-width:335px) and (max-width:420px) {

				

				header #suggestionbox {

    margin-left: 60px !important;

}







.attachdetailshome {

    margin-left: 220px;

}

				

				

			}

			

			

			



@media (max-width: 400px) {

.my-office-sec {

    margin-left: 70px !important;

}



}











@media (min-width:334px) and (max-width:400px) {





header #suggestionbox {margin-left:45px;}



#lnkLanguage { }



.thumb {margin-left:0% !important;}







.attachdetailshome {

    margin-left: 155px;

}









}	





@media (max-width: 375px) {

.my-office-sec {

   

}





}





@media (max-width: 360px) {

.my-office-sec {

    margin-left: 60px !important;

}









}





@media (min-width:321px) and (max-width:333px) {

	

		

		

		#lnkLanguage { }

		

		header #suggestionbox {margin-left:60px;}

		

		header #spnPostADTop {margin-left:100px;}

		

		

		

		adposteddetailshome {



    margin-left: 20px !important;

}





		.attachdetailshome {

    margin-left: 155px;

}

		

		.thumb {margin-left:-6% !important;}

		

		header #suggestionbox {margin-left:60px;}

		



		

		}

		



@media (max-width: 320px) {

.my-office-sec {

    margin-left: 33px !important;

}



#lnkLanguage { margin-top: -30px;}



}

	

	

	

	

</style>





                	<?php //echo "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                	if("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] == site_url() . "/my-account/"){

do_action( 'woocommerce_account_navigation' ); }
elseif((string)"http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] == (string) site_url() . "/ar/%D8%AD%D8%B3%D8%A7%D8%A8%D9%8A/" or  (string)"http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] == (string)site_url() . "/ar/%d8%ad%d8%b3%d8%a7%d8%a8%d9%8a/"){
	//echo "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
do_action( 'woocommerce_account_navigation' );
} ?>



<div class="woocommerce-MyAccount-content">

	<?php

		/**

		 * My Account content.

		 * @since 2.6.0

		 */

		do_action( 'woocommerce_account_content' );

	?>

</div>





	





	



	



