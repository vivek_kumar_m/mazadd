<?php



/**



 * My Account navigation



 *



 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/navigation.php.



 *



 * HOWEVER, on occasion WooCommerce will need to update template files and you



 * (the theme developer) will need to copy the new files to your theme to



 * maintain compatibility. We try to do this as little as possible, but it does



 * happen. When this occurs the version of the template file will be bumped and



 * the readme will list any important changes.



 *



 * @see     https://docs.woocommerce.com/document/template-structure/



 * @author  WooThemes



 * @package WooCommerce/Templates



 * @version 2.6.0



 */







if ( ! defined( 'ABSPATH' ) ) {



	exit;



}







do_action( 'woocommerce_before_account_navigation' );



?>





<style>



@media (max-width: 620px)



{

	

.bs-docs-footer {

    margin-right: 0px !important;

}	



	}

</style>


<?php if(apply_filters( 'wpml_current_language', NULL ) == 'ar' ){
?>

<div class="col-sm-4 my-office-sec">


			<a onclick="location.href = '<?php echo esc_url( site_url()); ?>/ar/حسابي/edit-account;'">
				<img src="<?php echo get_template_directory_uri(); ?>/Images\pro.png"/>



				



						<h5 style="margin-left:15px;text-decoration:none;"><?php echo _e('My Profile','mazadd'); ?> </h5>



					</a>



			</div>



			<div class="col-sm-4 my-office-sec">


			<a onclick="location.href = '<?php echo esc_url( site_url()); ?>/ar/إعلاناتي/'">
			<img src="<?php echo get_template_directory_uri(); ?>/Images\spkr.png"/>



				



					<h5 style="margin-left:45px;"><?php echo _e('My ADs','mazadd'); ?></h5>



					</a>



			</div>



			<div class="col-sm-4 my-office-sec">

				<a onclick="location.href = '<?php echo esc_url( site_url()); ?>/ar/حسابي/edit-address/'">

				<img src="<?php echo get_template_directory_uri(); ?>/Images\pro.png"/>



				



					<h5 style="margin-left:15px;"><?php echo _e('Edit Address ','mazadd'); ?></h5>



					</a>



			</div>





			<div class="col-sm-4 my-office-sec">


				<a href="<?php echo site_url(); ?>/ar/اضف-اعلان/">
				<img src="<?php echo get_template_directory_uri(); ?>/Images\fav.png"/>

						<h5 style="margin-left:15px;"><?php echo _e('Post an AD ','mazadd'); ?></h5>



					</a>



			</div>

			 <div class="col-sm-4 my-office-sec">
			 	<a onclick="location.href = '<?php echo esc_url( site_url()); ?>/ar/حسابي/messages/'">           
				<img src="<?php echo get_template_directory_uri(); ?>/Images\msg.png">     

					<h5 style="margin-left:15px;"><?php echo _e('Messages ','mazadd'); ?><small style="color:#ed2731; padding-left:13px; font-size:14px; font-weight:650;"> (<?php echo get_all_notifications(); ?>) </small></h5>



   					</a>



   			</div>

   			<div class="col-sm-4 my-office-sec">
   				<a href="<?php echo site_url(); ?>/ar/سلة-الشراء/">   

				<img src="<?php echo get_template_directory_uri(); ?>/Images\cart.png">     

					<h5 style="margin-left:15px;"><?php echo _e('Cart','mazadd'); ?><small style="color:#ed2731; padding-left:13px; font-size:14px; font-weight:650;"> (<?php echo  WC()->cart->get_cart_contents_count(); ?>) </small><h5>



   					</a>



   			</div>







			<div class="col-sm-4 my-office-sec">

				<a href="<?php echo site_url(); ?>/ar/قائمة-الطلبات/">

				<img src="<?php echo get_template_directory_uri(); ?>/Images\fav.png">                                     
				
				<h5 style="margin-left:16px;"><?php echo _e('Favourite','mazadd'); ?></h5>



											  </a>



			</div>



			 <div class="col-sm-4 my-office-sec">


			 	<a onclick="location.href = '<?php echo esc_url( site_url()); ?>/ar/حسابي/became-vendor/'">   
				<img src="<?php echo get_template_directory_uri(); ?>/Images\grp.png">  

				<h5 style="margin-left:16px;"><?php echo _e('Payment setup','mazadd'); ?> </h5>



                </a>



            </div>



           



			<div class="col-sm-4 my-office-sec">
				<a href="<?php echo site_url(); ?>/ar/بحث-في-الاعلانات/">
				<img src="<?php echo get_template_directory_uri(); ?>/Images\srch.png">                                    

				<h5 style="margin-left:18px;"><?php  _e( 'Search ADs','mazadd');?> <small style="color:#ed2731; padding-left:13px; font-size:14px; font-weight:650;"> (0) </small></h5>



				</a>



			</div>



			<div class="col-sm-4 my-office-sec">

				<a href="<?php echo site_url(); ?>/ar/shop/">   

				<img src="<?php echo get_template_directory_uri(); ?>/Images\auc.png"> 

				<h5 style="margin-left:20px;"><?php  _e( 'All Auctions','mazadd');?></h5>



				      </a>



			</div>







			<?php  if(is_super_admin( get_current_user_id() )){?>



			<div class="col-sm-4 my-office-sec">

				<a onclick="location.href = '<?php echo esc_url( site_url()); ?>/ar/حسابي/add-cities/'">

				<img src="<?php echo get_template_directory_uri(); ?>/Images\auc.png"> 

				<h5 style="margin-left:20px;"><?php echo _e('Add cities','mazadd'); ?></h5>



				      </a>



			</div>







			<?php } ?>







			<div class="col-sm-4 my-office-sec">

				<a href="<?php echo site_url(); ?>/ar/حسابي/">

				<img src="<?php echo get_template_directory_uri(); ?>/Images\stamp.png">                                     

				<h5 style="margin-left:26px;"><?php echo _e('Special Stamp','mazadd'); ?></h5>



											  </a>



			</div>



			 <div class="col-sm-4 my-office-sec">

			 	<a onclick="location.href = '<?php echo esc_url( site_url()); ?>/ar/حسابي/online-users/'">

				<img src="<?php echo get_template_directory_uri(); ?>/Images\grp.png">  


				<h5 style="margin-left:16px;"><?php echo _e('Online Users ','mazadd'); ?><small style="color:#ed2731; padding-left:13px; font-size:14px; font-weight:650;">(<?php echo count_onlineusers(); ?>)</small></h5>


                </a>



            </div>



			<div class="col-sm-4 my-office-sec">

				<a href="<?php echo site_url(); ?>/ar/حسابي/">

				<img src="<?php echo get_template_directory_uri(); ?>/Images\hand.png">  

				<h5 style="margin-left:18px;"><?php echo _e('Work Contract ','mazadd'); ?></h5><h5>



			   </h5>



			   </a>



			</div>



			<div class="col-sm-4 my-office-sec">


				<a href="<?php echo site_url(); ?>/ar/حسابي/">

				<img src="<?php echo get_template_directory_uri(); ?>/Images\partner.png">                      

				<h5 style="margin-left:16px;"><?php echo _e('Partner Office ','mazadd'); ?></h5><h5>



				           </h5>



				           </a>



			</div>

			<div class="col-sm-4 my-office-sec">

				<a href="<?php echo site_url(); ?>/ar/حسابي/">

				<img src="<?php echo get_template_directory_uri(); ?>/Images\sold.png">                                   

				<h5 style="margin-left:28px;"><?php echo _e('Sold ADs ','mazadd'); ?> <small style="color:#ed2731; padding-left:13px; font-size:14px; font-weight:650;"> (0) </small></h5><h5>



				                                </h5>



				</a>



			</div>



			<div class="col-sm-4 my-office-sec">


				<a href="<?php echo site_url(); ?>/ar/حسابي/">
				<img src="<?php echo get_template_directory_uri(); ?>/Images\rec.png">                                   

				<h5 style="margin-left:23px;"><?php echo _e('Received Amount ','mazadd'); ?><small style="color:#ed2731; padding-left:13px; font-size:14px; font-weight:650;"> (0) </small></h5><h5>



				                                </h5>



				                                </a>



			</div>	










<?php } else{ ?>
	








			<div class="col-sm-4 my-office-sec">


			<a href="<?php echo site_url(); ?>/my-account/edit-account">
				<img src="<?php echo get_template_directory_uri(); ?>/Images\pro.png"/>



				



						<h5 style="margin-left:15px;text-decoration:none;"><?php _e('My Profile','mazadd'); ?> </h5>



					</a>



			</div>



			<div class="col-sm-4 my-office-sec">


			<a href="<?php echo site_url(); ?>/my-ads/">
			<img src="<?php echo get_template_directory_uri(); ?>/Images\spkr.png"/>



				



					<h5 style="margin-left:45px;"><?php _e('My ADs','mazadd'); ?></h5>



					</a>



			</div>



			<div class="col-sm-4 my-office-sec">

				<a href="<?php echo site_url(); ?>/my-account/edit-address/">

				<img src="<?php echo get_template_directory_uri(); ?>/Images\pro.png"/>



				



					<h5 style="margin-left:15px;"><?php _e('Edit Address ','mazadd'); ?></h5>



					</a>



			</div>





			<div class="col-sm-4 my-office-sec">


				<a href="<?php echo site_url(); ?>/ad-post/">
				<img src="<?php echo get_template_directory_uri(); ?>/Images\fav.png"/>

						<h5 style="margin-left:15px;"><?php _e('Post an AD ','mazadd'); ?></h5>



					</a>



			</div>

			 <div class="col-sm-4 my-office-sec">
			 	<a href="<?php echo site_url(); ?>/my-account/messages/">           
				<img src="<?php echo get_template_directory_uri(); ?>/Images\msg.png">     

					<h5 style="margin-left:15px;"><?php _e('Messages ','mazadd'); ?><small style="color:#ed2731; padding-left:13px; font-size:14px; font-weight:650;"> (<?php echo get_all_notifications(); ?>) </small></h5>



   					</a>



   			</div>

   			<div class="col-sm-4 my-office-sec">
   				<a href="<?php echo site_url(); ?>/cart/">   

				<img src="<?php echo get_template_directory_uri(); ?>/Images\cart.png">     

					<h5 style="margin-left:15px;"><?php _e('Cart','mazadd'); ?><small style="color:#ed2731; padding-left:13px; font-size:14px; font-weight:650;"> (<?php echo  WC()->cart->get_cart_contents_count(); ?>) </small><h5>



   					</a>



   			</div>







			<div class="col-sm-4 my-office-sec">

				<a href="<?php echo site_url(); ?>/wishlist/">

				<img src="<?php echo get_template_directory_uri(); ?>/Images\fav.png">                                     
				
				<h5 style="margin-left:16px;"><?php _e('Favourite','mazadd'); ?></h5>



											  </a>



			</div>



			 <div class="col-sm-4 my-office-sec">


			 	<a href="<?php echo site_url(); ?>/my-account/became-vendor/">   
				<img src="<?php echo get_template_directory_uri(); ?>/Images\grp.png">  

				<h5 style="margin-left:16px;"><?php _e('Payment setup','mazadd'); ?> </h5>



                </a>



            </div>



           



			<div class="col-sm-4 my-office-sec">
				<a href="<?php echo site_url(); ?>/search/">
				<img src="<?php echo get_template_directory_uri(); ?>/Images\srch.png">                                    

				<h5 style="margin-left:18px;"><?php printf( __( 'Search ADs','mazadd'));?> <small style="color:#ed2731; padding-left:13px; font-size:14px; font-weight:650;"> (0) </small></h5>



				</a>



			</div>



			<div class="col-sm-4 my-office-sec">

				<a href="<?php echo site_url(); ?>/shop/">   

				<img src="<?php echo get_template_directory_uri(); ?>/Images\auc.png"> 

				<h5 style="margin-left:20px;"><?php printf( __( 'All Auctions','woocommerce'));?></h5>



				      </a>



			</div>







			<?php  if(is_super_admin( get_current_user_id() )){?>



			<div class="col-sm-4 my-office-sec">

				<a href="<?php echo site_url(); ?>/my-account/add-cities/">

				<img src="<?php echo get_template_directory_uri(); ?>/Images\auc.png"> 

				<h5 style="margin-left:20px;"><?php _e('Add cities','mazadd'); ?></h5>



				      </a>



			</div>







			<?php } ?>







			<div class="col-sm-4 my-office-sec">

				<a href="<?php echo site_url(); ?>/my-account/">

				<img src="<?php echo get_template_directory_uri(); ?>/Images\stamp.png">                                     

				<h5 style="margin-left:26px;"><?php _e('Special Stamp','mazadd'); ?></h5>



											  </a>



			</div>



			 <div class="col-sm-4 my-office-sec">

			 	<a href="<?php echo site_url(); ?>/my-account/online-users/">

				<img src="<?php echo get_template_directory_uri(); ?>/Images\grp.png">  


				<h5 style="margin-left:16px;"><?php echo _e('Online Users ','mazadd'); ?><small style="color:#ed2731; padding-left:13px; font-size:14px; font-weight:650;">(<?php echo count_onlineusers(); ?>)</small></h5>


                </a>



            </div>



			<div class="col-sm-4 my-office-sec">

				<a href="<?php echo site_url(); ?>/my-account/">

				<img src="<?php echo get_template_directory_uri(); ?>/Images\hand.png">  

				<h5 style="margin-left:18px;"><?php echo _e('Work Contract ','mazadd'); ?></h5><h5>



			   </h5>



			   </a>



			</div>



			<div class="col-sm-4 my-office-sec">


				<a href="<?php echo site_url(); ?>/my-account/">

				<img src="<?php echo get_template_directory_uri(); ?>/Images\partner.png">                      

				<h5 style="margin-left:16px;"><?php echo _e('Partner Office ','mazadd'); ?></h5><h5>



				           </h5>



				           </a>



			</div>

			<div class="col-sm-4 my-office-sec">

				<a href="<?php echo site_url(); ?>/my-account/">

				<img src="<?php echo get_template_directory_uri(); ?>/Images\sold.png">                                   

				<h5 style="margin-left:28px;"><?php echo _e('Sold ADs ','mazadd'); ?> <small style="color:#ed2731; padding-left:13px; font-size:14px; font-weight:650;"> (0) </small></h5><h5>



				                                </h5>



				</a>



			</div>



			<div class="col-sm-4 my-office-sec">


				<a href="<?php echo site_url(); ?>/my-account/">
				<img src="<?php echo get_template_directory_uri(); ?>/Images\rec.png">                                   

				<h5 style="margin-left:23px;"><?php _e('Received Amount ','mazadd'); ?><small style="color:#ed2731; padding-left:13px; font-size:14px; font-weight:650;"> (0) </small></h5><h5>



				                                </h5>



				                                </a>



			</div>		

			
	



					



<?php

} ?>

									







<?php do_action( 'woocommerce_after_account_navigation' ); ?>







					



					