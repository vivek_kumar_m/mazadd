<?php




function create_notification_table(){




global $wpdb;

$charset_collate = $wpdb->get_charset_collate();
$table_name = $wpdb->prefix.'auction_notification';
if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
$sql = "CREATE TABLE $table_name (
  id int(9) NOT NULL AUTO_INCREMENT,
  auction_id varchar(55) NOT NULL,
  user_id varchar(55)  NOT NULL,
  notification text  NOT NULL,
  notification_status varchar(55)  NOT NULL,
  PRIMARY KEY  (id)) $charset_collate;";
require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
dbDelta( $sql );

}



}




function create_message_table(){
  global $wpdb;
  $charset_collate = $wpdb->get_charset_collate();

  $table_name = $wpdb->prefix.'user_messages';

  if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {

  $sql = "CREATE TABLE $table_name (
    id int(9) NOT NULL AUTO_INCREMENT,
    user_id varchar(55)  NOT NULL,
    message text  NOT NULL,
    message_status varchar(55)  NOT NULL,
    PRIMARY KEY  (id)) $charset_collate;";
  require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
  dbDelta( $sql );

  }
}
function is_product_closed( $id){
$end_time = ($datetime = get_post_meta($id, '_yith_auction_to', true)) ? $datetime : NULL;

if ( $end_time ) {

    $date_to = $end_time;
   $date_now = strtotime('now');
    if ( $date_to <= $date_now){
        return TRUE;

    } else {
        return FALSE;
    }
} else {
    return TRUE;
}
}
/*notifications functions*/




function mazadd_get_max_bid($product_id){

 global $wpdb;




$table_name = $wpdb->prefix.'yith_wcact_auction';




$query   = $wpdb->prepare( "SELECT * FROM $table_name WHERE auction_id = %d ORDER by CAST(bid AS decimal(50,5)) DESC, date ASC LIMIT 1", $product_id );




$results = $wpdb->get_row( $query );









return $results;




        }




function Auction_Complete_notification(){
    $lan = new GoogleTranslate();
    $current_user = wp_get_current_user()->ID;
    global $wpdb;
    $table_name = $wpdb->prefix.'yith_wcact_auction';

    $query   = $wpdb->get_results( "SELECT DISTINCT auction_id FROM $table_name WHERE user_id = '$current_user' ORDER by date DESC ;" );
    foreach ($query as $value) {
    $productId = $value->auction_id;
    $max_bidder = mazadd_get_max_bid($productId);
if(is_product_closed( $productId)){
$table_name = $wpdb->prefix.'auction_notification';
$query = $wpdb->prepare( "SELECT * FROM $table_name WHERE auction_id = %d AND user_id = %d", $productId,$max_bidder->user_id );
$allmiles = $wpdb->get_var($query);
if($allmiles == null){
  $images = explode(",", get_post_meta($productId,'_product_image_gallery',true)) ;

      if(count($images)>0){

        if($images[0] ==""){
           $image = get_template_directory_uri().'/Images/mock.png';
         }else{
            $image = wp_get_attachment_image_url( $images[0],'full');
         }

     }else{

        $image = get_template_directory_uri().'/Images/mock.png';

    }
    if($my_current_lang = apply_filters( 'wpml_current_language', NULL ) == 'en' ){
    $price =get_post_meta($productId,'current_bid',true);
    $title = get_the_title($productId);
    $img = '<div class="text-center"><img src="'.$image.'" class="img-responsive" style="margin: 0 auto;"/></div>';
    $link= '<div class="text-center"><a href="'.get_permalink($productId).'" class="btn" style="background:#242424; padding:5px 40px; margin-top:50px; color:#fff;">Proceed to payment</a></div>';
$post_content='<p style="text-align:center; color:#949494; padding-top:10px;">You Win the Auction<strong>'.$title.'</strong></p>'.$img.''.$link;
$post_expert='You Win the Auction ';
$postarr =array (
   'post_type' => 'notification',
   'post_author'=> $max_bidder->user_id,
   'post_title' => 'Auction Completed',
   'post_content' => $post_content,
   'post_excerpt'=>$post_expert,
   'post_status' => 'publish',
   'comment_status' => 'closed',   // if you prefer
   'ping_status' => 'closed');
}else{
  $price =get_post_meta($productId,'current_bid',true);
    $title = get_the_title($productId);
    $img = '<div class="text-center"><img src="'.$image.'" class="img-responsive" style="margin: 0 auto;"/></div>';
    $link= '<div class="text-center"><a href="'.get_permalink($productId).'" class="btn" style="background:#242424; padding:5px 40px; margin-top:50px; color:#fff;">استمر لانهاء اجراء الدفع</a></div>';
$post_content='<p style="text-align:center; color:#949494; padding-top:10px;">لقد ربحت المزاد<strong>'.$title.'</strong></p>'.$img.''.$link;
$post_expert=' لقد ربحت المزاد';
$postarr =array (
   'post_type' => 'notification',
   'post_author'=> $max_bidder->user_id,
   'post_title' => 'انتهاء المزاد',
   'post_content' => $post_content,
   'post_excerpt'=>$post_expert,
   'post_status' => 'publish',
   'comment_status' => 'closed',   // if you prefer
   'ping_status' => 'closed');
}
$post_id=wp_insert_post($postarr, $wp_error = false );
$wpdb->insert( $table_name, array('auction_id'=>$productId,'user_id'=>$max_bidder->user_id,'notification'=>$post_id,'notification_status'=>0) , array( '%d', '%s','%d', '%d' )  );

}
}
else{
}

    }




}




function count_notifications(){
global $wpdb;
$current_user = wp_get_current_user()->ID;
$table_name = $wpdb->prefix.'auction_notification';
$query = $wpdb->get_results( "SELECT * FROM $table_name WHERE user_id ='$current_user' AND notification_status = '0';");
return count($query);
}
function send_messages($id,$title,$post_content){
global $wpdb;
$table_name = $wpdb->prefix.'user_messages';
$postarr =array (
   'post_type' =>'message',
   'post_author'=> wp_get_current_user()->ID,
   'post_title' =>  $title,
   'post_content' => $post_content,
   'post_status' => 'publish',
   'comment_status' => 'closed',   // if you prefer
   'ping_status' => 'closed');
$post_id=wp_insert_post($postarr, $wp_error = false );
$wpdb->insert( $table_name, array('user_id'=>$id,'message'=>$post_id,'message_status'=>0) , array( '%s','%d', '%d' )  );
}




function mazadd_get_second_max_bid($product_id){
  global $wpdb;
  $table_name = $wpdb->prefix.'yith_wcact_auction';
  $query   = $wpdb->prepare( "SELECT * FROM $table_name WHERE auction_id = %d ORDER by CAST(bid AS decimal(50,5)) DESC, date ASC LIMIT 2", $product_id );
  $results = $wpdb->get_row( $query );
  return $results;
}
function get_notifications_messages_id(){
  global $wpdb;
  $current_user = wp_get_current_user()->ID;
  $table_name = $wpdb->prefix.'auction_notification';
  $query = $wpdb->get_results( "SELECT notification FROM $table_name WHERE user_id ='$current_user' AND notification_status = '0';");
  return  $query;

}




function notification_posts(){
  $message_details=array();

  $message_ids = get_notifications_messages_id();

  foreach ($message_ids as $value) {

   $id = $value->notification;
  array_push($message_details,get_post($id));
  }
  return array_reverse($message_details);
}




function viewed_notifications_messages_id(){




global $wpdb;
$current_user = wp_get_current_user()->ID;
$table_name = $wpdb->prefix.'auction_notification';




$query = $wpdb->get_results( "SELECT notification FROM $table_name WHERE user_id ='$current_user' AND notification_status = '1';");




return  $query;




}




function viewed_notification_posts(){




$message_details=array();




$message_ids = viewed_notifications_messages_id();




foreach ($message_ids as $value) {




 $id = $value->notification;




array_push($message_details,get_post($id));




}




return array_reverse($message_details);









}




function update_notification_status(){




global $wpdb; 




$current_user = wp_get_current_user()->ID;




$table_name = $wpdb->prefix.'auction_notification';


  if($my_current_lang = apply_filters( 'wpml_current_language', NULL ) == 'en' ){ 

      $wpdb->update( 
    $table_name, array( 'notification_status' => 1), array( 'user_id' => $current_user,'notification' => get_the_ID(), 'notification_status'=>0 ), array( '%d'),array( '%d','%d','%d') 
    ); 

  }else{
        $wpdb->update( 
      $table_name, array( 'notification_status' => 1), array( 'user_id' => $current_user,'notification' => pll_get_post(get_the_ID(),'ar' ), 'notification_status'=>0 ), array( '%d'),array( '%d','%d','%d') 
      );
  }  
  




}




/*online users functions*/




function count_onlineusers(){




global $wpdb; 




$table_name = $wpdb->prefix.'online_user';



$current_user = wp_get_current_user()->ID;
$query = $wpdb->get_results( "SELECT * FROM $table_name WHERE online_status = '1' and user_id !='$current_user';");




return count($query);




}




function get_onlineusers(){




global $wpdb; 




$table_name = $wpdb->prefix.'online_user';



$current_user = wp_get_current_user()->ID;
$query = $wpdb->get_results( "SELECT * FROM $table_name WHERE online_status = '1' and user_id !='$current_user';");




return $query;




}




function get_count_messages(){




global $wpdb;




$table_name = $wpdb->prefix.'user_messages';




$current_user = wp_get_current_user()->ID;




$query = $wpdb->get_results( "SELECT * FROM $table_name WHERE user_id ='$current_user' AND message_status = '0';");




return  count($query);




}









function get_messages_id(){




global $wpdb;




$table_name = $wpdb->prefix.'user_messages';




$current_user = wp_get_current_user()->ID;




$query = $wpdb->get_results( "SELECT message FROM $table_name WHERE user_id ='$current_user' AND message_status = '0';");




return  $query;




}




function messages_posts(){




$message_details=array();




$message_ids = get_messages_id();




foreach ($message_ids as $value) {




 $id = $value->message;




array_push($message_details,get_post($id));




}




return array_reverse($message_details);




}




function get_readed_id(){




global $wpdb;




$table_name = $wpdb->prefix.'user_messages';




$current_user = wp_get_current_user()->ID;




$query = $wpdb->get_results( "SELECT message FROM $table_name WHERE user_id ='$current_user' AND message_status = '1';");




return  $query;




}




function readed_messages_posts(){




$message_details=array();




$message_ids = get_readed_id();




foreach ($message_ids as $value) {




 $id = $value->message;




array_push($message_details,get_post($id));




}




return array_reverse($message_details);




}




function update_messages_status(){




global $wpdb; 




$current_user = wp_get_current_user()->ID;




$table_name = $wpdb->prefix.'user_messages';




  $wpdb->update( 




$table_name, array( 'message_status' => 1), array( 'user_id' => $current_user,'message' => get_the_ID(), 'message_status'=>0 ), array( '%d'),array( '%d','%d','%d') 




);




}









/*all notifications*/









function get_all_notifications(){




$message = get_count_messages();




$notification = count_notifications();




$count = $message + $notification;




return $count;




}









function posttt(){




$postarr =array (




   'post_type' => 'message',




   'post_author'=> get_current_user_id(),




   'post_title' => $_POST['post_name'],




   'post_content' => $_POST['post_content'],




   'post_status' => 'private',




   'comment_status' => 'closed',   // if you prefer




   'ping_status' => 'closed',




    'meta_input'    => array('_price'   =>  $_POST['price']) );




$post_id=wp_insert_post($postarr, $wp_error = false );




}














/*




 * Step 1. Add Link to My Account menu




 */




add_filter ( 'woocommerce_account_menu_items', 'messages_link', 40 );




function messages_link( $menu_links ){




 




$menu_links = array_slice( $menu_links, 0, 5, true ) 




+ array( 'messages' => __('User Messages','mazadd'),




'became-vendor' => __('Became vendor','mazadd'),




'add-cities' => __('Add Cities','mazadd'),




'online-users' => __('Online users','mazadd') )




+ array_slice( $menu_links, 5, NULL, true );




 




return $menu_links;




 




}




/*




 * Step 2. Register Permalink Endpoint




 */




add_action( 'init', 'messages_add_endpoint' );

function messages_add_endpoint() {

// WP_Rewrite is my Achilles' heel, so please do not ask me for detailed explanation
add_rewrite_endpoint( __('messages','mazadd'), EP_PAGES );
add_rewrite_endpoint( __('became-vendor','mazadd'), EP_PAGES );
add_rewrite_endpoint( __('add-cities','mazadd'), EP_PAGES );
add_rewrite_endpoint( __('online-users','mazadd'), EP_PAGES );

}




/*




 * Step 3. Content for the new page in My Account, woocommerce_account_{ENDPOINT NAME}_endpoint




 */




add_action( 'woocommerce_account_messages_endpoint', 'my_messages_endpoint_content' );




function my_messages_endpoint_content() {




 




// of course you can print dynamic content here, one of the most useful functions here is get_current_user_id()




wc_get_template( 'myaccount/messages.php');




 




}




add_action( 'woocommerce_account_became-vendor_endpoint', 'became_vendor_endpoint_content' );




function became_vendor_endpoint_content() {




 




// of course you can print dynamic content here, one of the most useful functions here is get_current_user_id()




wc_get_template( 'myaccount/became-vendor.php');




 




}




add_action( 'woocommerce_account_online-users_endpoint', 'online_users_endpoint_content' );




function online_users_endpoint_content() {




 




// of course you can print dynamic content here, one of the most useful functions here is get_current_user_id()




wc_get_template( 'myaccount/online-users.php');




 




}









add_action( 'woocommerce_account_add-cities_endpoint', 'add_cities_endpoint_content' );




function add_cities_endpoint_content() {




 




?>




<div class="row">




           <div class="col-sm-6 login-blk">




           <form method="post">




           <div class="form-group">




           <label>ADD Cities</label>




           </div>




           <div class="form-group">




           <select name="country" class="form-control" onchange="selectCountry(this.value);" >




    <option selected="selected" value="">-- Select --</option>




    <?php global $woocommerce;




    $countries_obj   = new WC_Countries();




    $countries   = $countries_obj->__get('countries');




    foreach ($countries as $key => $value) {




        echo '<option value="'.$key.'">'.$value.'</option>';




    } 




    ?>




</select>




        </div>




        <div class="form-group">




           <input type="text" name="cities_name" class="form-control">




           </div>




           <div class="form-group">




           <input type="submit" name="add_cities">




           </div>




           </form>




</div>




<div class="col-sm-6">




           <form method="post">




           <div class="form-group">




           <label>ADD Region</label>




           </div>




           <div class="form-group">




           <select name="rcountry" class="form-control" onchange="selectCountry(this.value);" >




    <option selected="selected" value="">-- Select --</option>




    <?php global $woocommerce;




    $countries_obj   = new WC_Countries();




    $countries   = $countries_obj->__get('countries');




    foreach ($countries as $key => $value) {




        echo '<option value="'.$key.'">'.$value.'</option>';




    } 




    ?>




</select>




        </div>




        <div class="form-group">




           <input type="text" name="region_name" class="form-control">




           </div>




           <div class="form-group">




           <input type="submit" name="add_region" >




           </div>




           </form>




</div>




</div>




<?php




global $wpdb;




if(isset($_POST['add_cities'])){




    $wpdb->insert( 'wp_monadie_cities', array('City_Name'=>$_POST['cities_name'],'Country_Code'=>$_POST['country']) , array( '%s', '%s' )  );









  }




  if(isset($_POST['add_region'])){




    $wpdb->insert( 'wp_monadie_regions', array('Region_Name'=>$_POST['region_name'],'Country_Code'=>$_POST['rcountry']) , array( '%s', '%s' )  ); 









  }




 




}




/*




 * Step 4




 */




// Go to Settings > Permalinks and just push "Save Changes" button.