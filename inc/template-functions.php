<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package mazadd
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function mazadd_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'mazadd_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function mazadd_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'mazadd_pingback_header' );
// Register Custom Post Type
function mazzad_lost_items() {

	$labels = array(
		'name'                  => _x( 'Lost items', 'Post Type General Name', 'mazzad' ),
		'singular_name'         => _x( 'Lost item', 'Post Type Singular Name', 'mazzad' ),
		'menu_name'             => __( 'Lost items', 'mazzad' ),
		'name_admin_bar'        => __( 'Lost item', 'mazzad' ),
		'archives'              => __( 'Item Archives', 'mazzad' ),
		'attributes'            => __( 'Item Attributes', 'mazzad' ),
		'parent_item_colon'     => __( 'Parent Item:', 'mazzad' ),
		'all_items'             => __( 'All Items', 'mazzad' ),
		'add_new_item'          => __( 'Add New Item', 'mazzad' ),
		'add_new'               => __( 'Add New', 'mazzad' ),
		'new_item'              => __( 'New Item', 'mazzad' ),
		'edit_item'             => __( 'Edit Item', 'mazzad' ),
		'update_item'           => __( 'Update Item', 'mazzad' ),
		'view_item'             => __( 'View Item', 'mazzad' ),
		'view_items'            => __( 'View Items', 'mazzad' ),
		'search_items'          => __( 'Search Item', 'mazzad' ),
		'not_found'             => __( 'Not found', 'mazzad' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'mazzad' ),
		'featured_image'        => __( 'Featured Image', 'mazzad' ),
		'set_featured_image'    => __( 'Set featured image', 'mazzad' ),
		'remove_featured_image' => __( 'Remove featured image', 'mazzad' ),
		'use_featured_image'    => __( 'Use as featured image', 'mazzad' ),
		'insert_into_item'      => __( 'Insert into item', 'mazzad' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'mazzad' ),
		'items_list'            => __( 'Items list', 'mazzad' ),
		'items_list_navigation' => __( 'Items list navigation', 'mazzad' ),
		'filter_items_list'     => __( 'Filter items list', 'mazzad' ),
	);
	$args = array(
		'label'                 => __( 'Lost item', 'mazzad' ),
		'description'           => __( 'Post Type Description', 'mazzad' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array(),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'lost_items', $args );

}
add_action( 'init', 'mazzad_lost_items', 0 );
// Register Custom Post Type
function mazzad_messages() {

	$labels = array(
		'name'                  => _x( 'Messages', 'Post Type General Name', 'unique' ),
		'singular_name'         => _x( 'Message', 'Post Type Singular Name', 'unique' ),
		'menu_name'             => __( 'Message', 'unique' ),
		'name_admin_bar'        => __( 'Message', 'unique' ),
		'archives'              => __( 'Message Archives', 'unique' ),
		'attributes'            => __( 'Message Attributes', 'unique' ),
		'parent_item_colon'     => __( 'Parent Message:', 'unique' ),
		'all_items'             => __( 'All Messages', 'unique' ),
		'add_new_item'          => __( 'Add New Message', 'unique' ),
		'add_new'               => __( 'Add New', 'unique' ),
		'new_item'              => __( 'New Item', 'unique' ),
		'edit_item'             => __( 'Edit Item', 'unique' ),
		'update_item'           => __( 'Update Item', 'unique' ),
		'view_item'             => __( 'View Item', 'unique' ),
		'view_items'            => __( 'View Items', 'unique' ),
		'search_items'          => __( 'Search Item', 'unique' ),
		'not_found'             => __( 'Not found', 'unique' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'unique' ),
		'featured_image'        => __( 'Featured Image', 'unique' ),
		'set_featured_image'    => __( 'Set featured image', 'unique' ),
		'remove_featured_image' => __( 'Remove featured image', 'unique' ),
		'use_featured_image'    => __( 'Use as featured image', 'unique' ),
		'insert_into_item'      => __( 'Insert into item', 'unique' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'unique' ),
		'items_list'            => __( 'Items list', 'unique' ),
		'items_list_navigation' => __( 'Items list navigation', 'unique' ),
		'filter_items_list'     => __( 'Filter items list', 'unique' ),
	);
	$args = array(
		'label'                 => __( 'Message', 'unique' ),
		'description'           => __( 'Post Type Description', 'unique' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
		'taxonomies'            => array( '' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-tickets-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'message', $args );

}
add_action( 'init', 'mazzad_messages', 0 );
function mazzad_Notification() {

	$labels = array(
		'name'                  => _x( 'Notifications', 'Post Type General Name', 'unique' ),
		'singular_name'         => _x( 'Notification', 'Post Type Singular Name', 'unique' ),
		'menu_name'             => __( 'Notification', 'unique' ),
		'name_admin_bar'        => __( 'Notification', 'unique' ),
		'archives'              => __( 'Notification Archives', 'unique' ),
		'attributes'            => __( 'Notification Attributes', 'unique' ),
		'parent_item_colon'     => __( 'Parent Notification:', 'unique' ),
		'all_items'             => __( 'All Notification', 'unique' ),
		'add_new_item'          => __( 'Add New Notification', 'unique' ),
		'add_new'               => __( 'Add New', 'unique' ),
		'new_item'              => __( 'New Item', 'unique' ),
		'edit_item'             => __( 'Edit Item', 'unique' ),
		'update_item'           => __( 'Update Item', 'unique' ),
		'view_item'             => __( 'View Item', 'unique' ),
		'view_items'            => __( 'View Items', 'unique' ),
		'search_items'          => __( 'Search Item', 'unique' ),
		'not_found'             => __( 'Not found', 'unique' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'unique' ),
		'featured_image'        => __( 'Featured Image', 'unique' ),
		'set_featured_image'    => __( 'Set featured image', 'unique' ),
		'remove_featured_image' => __( 'Remove featured image', 'unique' ),
		'use_featured_image'    => __( 'Use as featured image', 'unique' ),
		'insert_into_item'      => __( 'Insert into item', 'unique' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'unique' ),
		'items_list'            => __( 'Items list', 'unique' ),
		'items_list_navigation' => __( 'Items list navigation', 'unique' ),
		'filter_items_list'     => __( 'Filter items list', 'unique' ),
	);
	$args = array(
		'label'                 => __( 'Notification', 'unique' ),
		'description'           => __( 'Post Type Description', 'unique' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
		'taxonomies'            => array( '' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-tickets-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'notification', $args );

}
add_action( 'init', 'mazzad_Notification', 0 );